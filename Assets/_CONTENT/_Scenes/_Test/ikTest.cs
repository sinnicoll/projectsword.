using System.Collections;
using System.Collections.Generic;
using UnityEngine.Animations.Rigging;
using UnityEngine;

public class ikTest : MonoBehaviour
{

    public GameObject target;
    public GameObject boneRaycast;
    public Rig rig;

    public float interpSpeed = 2.0f;
    public float distaceIk = 1.0f;
    
    float startTime;
    Vector3 ogPosition;
    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        ogPosition = target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        //layerMask = ~layerMask;

        RaycastHit hit;

        float t = (Time.time - startTime) / interpSpeed;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(boneRaycast.transform.position, boneRaycast.transform.right * -1, out hit, distaceIk, layerMask))
        {
            rig.weight = Mathf.Lerp(rig.weight, 0.6f, Time.deltaTime * interpSpeed);
            Debug.DrawRay(boneRaycast.transform.position, boneRaycast.transform.right * -distaceIk, Color.green);
            Debug.Log("Did Hit");
            target.transform.position = Vector3.Lerp(target.transform.position, hit.transform.position, Time.deltaTime * interpSpeed);
        }
        else
        {
            rig.weight = Mathf.Lerp(rig.weight, 0, Time.deltaTime * interpSpeed);
            Debug.DrawRay(boneRaycast.transform.position, boneRaycast.transform.right * -distaceIk, Color.red);
            Debug.Log("Did not Hit");
            target.transform.position = Vector3.Lerp(target.transform.position,ogPosition, Time.deltaTime * interpSpeed);
        }


        //-------------------
        //RaycastHit hit;

        //int layermask = 1 << 8;
        //if (Physics.Raycast(transform.position, boneRaycast.transform.forward, 10)) print("object");
        //Debug.DrawRay(transform.position, boneRaycast.transform.right * 10, Color.green);
    }
}
