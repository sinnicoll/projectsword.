using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_TestTriggerEnemyGroup : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            Transform[] children = transform.GetChildren(true);
            foreach (Transform child in children)
            {
                child.gameObject.SetActive(true);
                child.parent = null;
            }
            gameObject.SetActive(false);
        }
    }
}
