using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_PauseCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       this.GetComponent<Camera>().transform.parent = Camera.main.transform;
       this.GetComponent<Camera>().transform.SetPositionAndRotation(this.GetComponent<Camera>().transform.parent.position, this.GetComponent<Camera>().transform.parent.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
