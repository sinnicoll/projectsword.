using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CS_HUD : MonoBehaviour
{
    public static CS_HUD instance = null;

    [SerializeField] Animator      animator;

    [Header("Event System")]
    [SerializeField] EventSystem   eventSystem;

    [Header("Pause")]
    [SerializeField] GameObject pause_component;
    [SerializeField] GameObject pause_FirstSelected;

    [SerializeField] TextMeshProUGUI missionText;
    [SerializeField] Image missionBackground;

    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip   continueAudioClip;
    public AudioClip[] slashAudioClips;
    public AudioClip   deathAudioClip;

    int previousSwingEffectID;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Locked;

        Time.timeScale   = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)         ) ShowPauseMenu();
        if (Input.GetKeyDown(KeyCode.JoystickButton7)) ShowPauseMenu();
        if (Input.GetKeyUp(KeyCode.Z))                 OnPlayerDeath();
    }

    public void ShowPauseMenu() 
    {
        if (pause_component.activeInHierarchy) 
        {
            OnContinue();
        }

        else
        {
            OnPause();
            Time.timeScale = 0.0f;

            if (GameManager.instance.currentInput == InputScheme.Keyboard) 
            {
                //Cursor.lockState = CursorLockMode.None;
                Cursor.lockState = CursorLockMode.Confined;
            }
            else
            eventSystem.SetSelectedGameObject(pause_FirstSelected); 
        }
    }
    public void OnPlayerDeath() 
    {
        audioSource.PlayOneShot(deathAudioClip);
        animator.SetTrigger("Death");
    }
    public void OnReloadCheckpoint() 
    {
        PlayRandomSlashSFX();
        animator.SetTrigger("CheckpointPause");
    }

    public void HidePauseMenu() 
    {
        animator.SetTrigger("HidePause");
    }

    public void OnContinue() 
    {
        //Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Locked;

        eventSystem.SetSelectedGameObject(null);
        animator.SetTrigger("ContinuePause");

        audioSource.PlayOneShot(continueAudioClip);
    }
    public void OnPause()
    {
        PlayRandomSlashSFX();
        animator.SetTrigger("ShowPause");
        pause_component.SetActive(true);
    }
    public void OnMainMenu() 
    {
        PlayRandomSlashSFX();
        animator.SetTrigger("ExitPause");
    }

    public void OnEndStage() 
    {
        animator.SetTrigger("Exit");
    }

    #region Aux Functions
    public void PlayRandomSlashSFX() 
    {
        int newSwingEffectID;

        if (previousSwingEffectID + 1 < slashAudioClips.Length) newSwingEffectID = previousSwingEffectID + 1;
        else newSwingEffectID = 0;

        audioSource.PlayOneShot(slashAudioClips[newSwingEffectID]);

        previousSwingEffectID = newSwingEffectID;
    }

    #endregion
}
