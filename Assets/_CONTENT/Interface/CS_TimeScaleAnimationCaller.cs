using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_TimeScaleAnimationCaller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopTimescale() 
    {
        Time.timeScale = 0;
    }

    public void ResumeTimescale() 
    {
        Time.timeScale = 1.0f;
    }

    public void BackToMainMenu() 
    {
        CS_SceneManager.instance.BackToMainMenu();
    }

    public void ReloadScene()
    {
        CS_SceneManager.instance.ReloadScene();
    }

    public void LoadNextScene() 
    {
    
    }
}
