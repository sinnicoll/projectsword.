using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CS_MainMenu : MonoBehaviour
{
    [SerializeField] EventSystem eventSystem;

    [Header("Menu Components")]
    [SerializeField] GameObject component_Landing;
    [SerializeField] GameObject component_Main;
    [SerializeField] GameObject component_Settings;

    [Header("First Selected")]
    [SerializeField] GameObject firstSelected_Main;
    [SerializeField] GameObject firstSelected_Settings;

    [Header("References")]
    [SerializeField] Animator sliceAnimator;

    [Header("Audio Elements")]
    [SerializeField] AudioSource FxAudioSource;
    [SerializeField] AudioClip[]   sliceEffect;

    InputScheme cachedInputScheme;
    int previousSwingEffectID;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.instance.landingShown) 
        {   
            HideLanding();
            ShowMain();
            HideSettings();
        }
        else 
        {            
            GameManager.instance.landingShown = true;
            ShowLanding();
            HideMain();
            HideSettings();  
        }

        previousSwingEffectID = 0;
        GameManager.instance.destroyedComponents.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        if(component_Landing.activeInHierarchy && Input.anyKey) 
        {
            SliceMenu("Landing");

            HideLanding();
            ShowMain();
        }
        
        if(cachedInputScheme != GameManager.instance.currentInput) 
        {
            if (GameManager.instance.currentInput == InputScheme.Keyboard) 
            {
                eventSystem.SetSelectedGameObject(null); 
            }

            else if (GameManager.instance.currentInput == InputScheme.Controller)
            {
                if (component_Main.activeInHierarchy)
                    eventSystem.SetSelectedGameObject(firstSelected_Main); 

                else if(component_Settings.activeInHierarchy)
                    eventSystem.SetSelectedGameObject(firstSelected_Settings);
            }
        }
        cachedInputScheme = GameManager.instance.currentInput;
    }

    public void HideMain()     { component_Main.SetActive(false);  }
    public void ShowMain()     
    { 
        component_Main.SetActive(true);   

        if(GameManager.instance.currentInput == InputScheme.Controller)
        eventSystem.SetSelectedGameObject(firstSelected_Main);
    }

    public void HideSettings() { component_Settings.SetActive(false); }
    public void ShowSettings() 
    {
        component_Settings.SetActive(true);

        if (GameManager.instance.currentInput == InputScheme.Controller)
        eventSystem.SetSelectedGameObject(firstSelected_Settings);
    }

    public void HideLanding()  { component_Landing.SetActive(false);  }
    public void ShowLanding()  { component_Landing.SetActive(true);  }

    public void SliceMenu(string sliceName)
    {
        PlayRandomSwingEffect();
        sliceAnimator.SetTrigger(sliceName); 
    }

    private void PlayRandomSwingEffect() 
    {
        int newSwingEffectID;

        if (previousSwingEffectID + 1 < sliceEffect.Length) newSwingEffectID = previousSwingEffectID + 1;
        else newSwingEffectID = 0;

        FxAudioSource.PlayOneShot(sliceEffect[newSwingEffectID]);

        previousSwingEffectID = newSwingEffectID;
    }
}
