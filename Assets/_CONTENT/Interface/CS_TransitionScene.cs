using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CS_TransitionScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.instance.currentFlowScene.Contains("MainMenu"))
            CS_SceneManager.instance.LoadSceneAsync("SC_LVL01_Logic","SC_LVL01_Meshes","SC_LVL01_Lighting");

        if(GameManager.instance.currentFlowScene.Contains("LVL01"))
            CS_SceneManager.instance.LoadSceneAsync("SC_LVL02_Logic","SC_LVL02_Lighting","SC_LVL02_Meshes");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
