using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class CS_SceneManager : MonoBehaviour
{
    public static CS_SceneManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //public void DelayedRespawn() 
    //{
    //    StartCoroutine(DelayedRespawnCoroutine());
    //}
    //IEnumerator DelayedRespawnCoroutine() 
    //{
    //    yield return new WaitForSeconds(1.0f);
    //    ReloadScene();
    //}
    public void ContinueGame() 
    {
        SceneManager.LoadScene("SC_TransitionScene");
    }

    public void ReloadScene() 
    {
        if(GameManager.instance.currentFlowScene != "") 
        {
            SceneManager.UnloadSceneAsync(GameManager.instance.currentFlowScene);
            SceneManager.LoadScene       (GameManager.instance.currentFlowScene, LoadSceneMode.Additive);

            GameManager.instance.destroyedComponents.Clear();        
        }
        else 
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("SC_MainMenu", LoadSceneMode.Single);
    }

    //Loads One or Multiple scene Async by String
    public void LoadSceneAsync( string sceneName00, string sceneName01 = null, string sceneName02 = null, string sceneName03 = null) 
    {
        SceneManager.LoadScene(sceneName00, LoadSceneMode.Single);

        if (sceneName01 != null)  SceneManager.LoadScene(sceneName01, LoadSceneMode.Additive);
        if (sceneName02 != null)  SceneManager.LoadScene(sceneName02, LoadSceneMode.Additive);
        if (sceneName03 != null)  SceneManager.LoadScene(sceneName03, LoadSceneMode.Additive);
    }

    public void UnloadSceneAsync( Scene scene ) 
    {
        SceneManager.UnloadSceneAsync(scene);
    }

    public void TransitionToNextLevel(string newScene) 
    {
        SceneManager.LoadSceneAsync("SC_TransitionScene");
    }

    public void ClearStages() 
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i).name.Contains("Stage"))
                SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
        }
    }
}
