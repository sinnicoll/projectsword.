using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CS_LevelManager : MonoBehaviour
{
    public static CS_LevelManager instance = null;

    [Header("Level Music")]
    [SerializeField] string musicOnStart;

    [Header("Scene Flow")]
    public string       flowSceneName;

    [Header("Mission")]
    public bool missionAccomplished;

    [Header("Mission Objectives")]
    public int  killEnemiesAmount;
    public int  currentEnemiesKilled;

    public UnityEvent OnMissionAccomplishedEvent;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        //else if (instance != this)
        //    Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.currentFlowScene = flowSceneName;
        CS_SceneManager.instance.ClearStages();

        if (musicOnStart != "")
        {
            CS_AudioManager.instance.PlayMusicElement(musicOnStart);
        }
    }
    private void Update()
    {
        if (killEnemiesAmount > 0)
        if (currentEnemiesKilled >= killEnemiesAmount && !missionAccomplished) OnMissionAccomplished();  
    }

    void OnMissionAccomplished() 
    {
        missionAccomplished = true;

        CS_AudioManager.instance.StopMusicElement(false);
        CS_AudioManager.instance.PlayGenericSFX("SFX_RecordScratch");
        CS_AudioManager.instance.PlayMusicElement("M_Heartbeat");

        OnMissionAccomplishedEvent.Invoke();
    }
}
