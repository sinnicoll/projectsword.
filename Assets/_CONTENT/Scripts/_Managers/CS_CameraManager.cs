using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Invector.vCamera;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CS_CameraManager : MonoBehaviour
{
    public static CS_CameraManager instance;

    [Header("Postprocess")]
    [SerializeField] Volume vol;
    [SerializeField] float  vignetteMaxIntensity;
    [SerializeField] float  chromaticAbMaxIntensity;

    [Header("Parent Camera")]
    vThirdPersonCamera thirdPersonCamera;

    //[Header("Lock Camera")]
    //[SerializeField] CS_LockOn lockComponent;

    DepthOfField dofComponent;
    Vignette vigComponent;
    ChromaticAberration chaComponent;

    //Local Values
    bool isRunning;
    bool isCutting;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        thirdPersonCamera = transform.parent.gameObject.GetComponent<vThirdPersonCamera>();

        DepthOfField dtmp;
        if (vol.profile.TryGet<DepthOfField>(out dtmp))
        {
            dofComponent = dtmp;
        }

        Vignette vtmp;
        if (vol.profile.TryGet<Vignette>(out vtmp))
        {
            vigComponent = vtmp;
        }

        ChromaticAberration ctmp;
        if (vol.profile.TryGet<ChromaticAberration>(out ctmp))
        {
            chaComponent = ctmp;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isRunning)
        {
            if (chaComponent.intensity.value < chromaticAbMaxIntensity)
                chaComponent.intensity.value += Time.deltaTime;
        }
        else
        {
            if (chaComponent.intensity.value > 0)
                chaComponent.intensity.value -= Time.deltaTime;
        }

        if (isCutting)
        {
            if (vigComponent.intensity.value < vignetteMaxIntensity)
                vigComponent.intensity.value += Time.deltaTime;
        }
        else
        {
            if (vigComponent.intensity.value > 0)
                vigComponent.intensity.value -= Time.deltaTime;
        }
    }

    public void FacsPostprocess(bool isActive)
    {
        isCutting = isActive;

        if (isActive)
            dofComponent.active = true;

        else
            dofComponent.active = false;
    }

    public void RunningPostprocess(bool isActive)
    {
        isRunning = isActive;

        if (isActive)
            thirdPersonCamera.ChangeState("Running");

        else
            if (thirdPersonCamera.currentStateName == "Running")
                thirdPersonCamera.ChangeState("Default");
    }
}
