using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;

public class CS_AudioManager : MonoBehaviour
{
    public static CS_AudioManager instance = null;
    public AudioMixer audioMixer;

    [Header("AudioParameters")]
    [Range(0, 1)]
    public float musicVolume;

    [Header("Music Themes")]
    public CS_AudioElement[] musicAudioElements;
    [HideInInspector] public AudioSource musicElementSource;
    CS_AudioElement currentMusic;

    [Header("General SFX")]
    public CS_AudioElement[] sfxElements;
    [HideInInspector] public AudioSource sfxAudioSource;

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        // MUSIC SOURCE 
        musicElementSource = gameObject.AddComponent<AudioSource>();
        musicElementSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("Music")[0];
        musicElementSource.priority    = 0;
        musicElementSource.minDistance = 1;
        musicElementSource.maxDistance = 1;
        musicElementSource.playOnAwake = false;
        musicElementSource.loop        = true;

        // SFX SOURCE 
        sfxAudioSource = gameObject.AddComponent<AudioSource>();
        sfxAudioSource.outputAudioMixerGroup = audioMixer.FindMatchingGroups("SFX")[0];
        sfxAudioSource.priority    = 0;
        sfxAudioSource.minDistance = 1;
        sfxAudioSource.maxDistance = 1;
        sfxAudioSource.playOnAwake = false;
        sfxAudioSource.loop        = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        //MUSIC
        if (musicVolume > 0.05f)
            audioMixer.SetFloat("MusicVolume", Mathf.Lerp(-30, 0, musicVolume));
        else
            audioMixer.SetFloat("MusicVolume", -99);
    }

    #region Music Functions
    //Will force play a new song
    public void PlayMusicElement(string name)
    {
        if (name != "")
        {
            if (currentMusic != null && currentMusic.name == name) { return; }

            CS_AudioElement audio = Array.Find(musicAudioElements, audioelement => audioelement.name == name);

            if (audio == null) { return; }

            else
            {
                audio.audioSource      = musicElementSource;
                audio.audioSource.clip = audio.audioClip;

                if (currentMusic != null)
                {
                    StartCoroutine(FadeSongs(currentMusic, audio));
                }

                else
                {
                    audio.audioSource.pitch = audio.pitch;
                    audio.audioSource.loop = audio.loop;

                    StartCoroutine(FadeInOnPlay(audio));
                    currentMusic = audio;
                }
            }
        }
    }
    public void PauseMusicElement()
    {
        if (currentMusic != null)
            currentMusic.audioSource.Pause();
    }
    public void ResumeMusicElement()
    {
        if (currentMusic != null)
            currentMusic.audioSource.Play();
    }
    public void StopMusicElement(bool withFade)
    {
        if (currentMusic != null && withFade) StartCoroutine(FadeOutOnStop());
        if (currentMusic != null && !withFade)
        {
            if (currentMusic.audioSource != null)
            {
                currentMusic.audioSource.Stop();
                currentMusic = null;
            }
        }
    }
    #endregion

    #region SFX Functions

    public void PlayGenericSFX(string name) 
    {
        if (name != "")
        {
            CS_AudioElement audio = Array.Find(sfxElements, audioelement => audioelement.name == name);
            if (audio == null) { return; }

            else
            {
                audio.audioSource      = sfxAudioSource;
                audio.audioSource.clip = audio.audioClip;

                audio.audioSource.Play();
            }
        }
    }

    #endregion

    #region Aux Functions
    IEnumerator FadeInOnPlay(CS_AudioElement musicToPlay)
    {
        musicToPlay.audioSource.volume    = 0;
        float maxFadeInOnPlayDuration     = 5.0F;
        float currentFadeInOnPlayDuration = 0;

        //Zero Volume and Play Song
        musicToPlay.audioSource.volume = 0;
        musicToPlay.audioSource.Play();

        //Volume goes up to limit in the indicated time
        while (currentFadeInOnPlayDuration < maxFadeInOnPlayDuration)
        {
            currentFadeInOnPlayDuration   += Time.fixedDeltaTime;
            musicToPlay.audioSource.volume = Mathf.Lerp(0, musicToPlay.volume, currentFadeInOnPlayDuration / maxFadeInOnPlayDuration);
            yield return null;
        }

        //Volume confirmed at max volume
        musicToPlay.audioSource.volume = musicToPlay.volume;
        yield return new WaitForEndOfFrame();
    }
    IEnumerator FadeOutOnStop()
    {
        float maxFadeOutOnStopDuration = 0.5F;
        float currentFadeOutOnStopDuration = 0;

        if (currentMusic != null && currentMusic.audioSource != null)
        {
            while (currentFadeOutOnStopDuration < maxFadeOutOnStopDuration)
            {
                currentFadeOutOnStopDuration += Time.fixedDeltaTime;

                if (currentMusic != null && currentMusic.audioSource != null)
                    currentMusic.audioSource.volume = Mathf.Lerp(currentMusic.audioSource.volume, 0, currentFadeOutOnStopDuration / maxFadeOutOnStopDuration);

                yield return null;
            }

            if (currentMusic != null && currentMusic.audioSource != null)
            {
                currentMusic.audioSource.Stop();
                currentMusic = null;
            }
        }
        yield return new WaitForEndOfFrame();
    }
    IEnumerator FadeSongs(CS_AudioElement originalAudio, CS_AudioElement newAudio)
    {
        float maximumVolume = newAudio.volume;

        float maxfadeOutDuration     = 0.5F;
        float currentFadeOutDuration = 0;

        float maxfadeInDuration      = 0.5F;
        float currentFadeInDuration  = 0;

        //Fades out the First Song
        while (currentFadeOutDuration < maxfadeOutDuration)
        {
            currentFadeOutDuration += Time.unscaledDeltaTime;

            if (originalAudio.audioSource != null)
                originalAudio.audioSource.volume = Mathf.Lerp(originalAudio.volume, 0, (currentFadeOutDuration / maxfadeOutDuration));
            yield return null;
        }

        //Stops the first song and returns it's value to the original, plays the next song
        StopMusicElement(false);

        newAudio.audioSource      = musicElementSource;
        newAudio.audioSource.clip = newAudio.audioClip;

        newAudio.audioSource.volume = 0;
        newAudio.audioSource.Play();
        currentMusic = newAudio;

        //Fades in the Second Song
        while (currentFadeInDuration < maxfadeInDuration)
        {
            currentFadeInDuration += Time.unscaledDeltaTime;
            newAudio.audioSource.volume = Mathf.Lerp(0, maximumVolume, currentFadeInDuration / maxfadeInDuration);

            yield return null;
        }

        yield return new WaitForEndOfFrame();
    }
    #endregion
}