using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [Header("Input")]
    public InputScheme currentInput;

    [Header("Game Variables")]
    public bool landingShown;

    public string currentFlowScene;

    public List<int> destroyedComponents;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(this);

        if (!instance.player)
            instance.player = GameObject.FindGameObjectWithTag("Player");

        landingShown = false;
    }

    public GameObject player;
    private void Start()
    {
        //Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;
    }
    private void Update()
    {

    }

    public void DelayedQuit(float delay) 
    {
        StartCoroutine(DelayedQuitCoroutine(delay));
    }

    IEnumerator DelayedQuitCoroutine(float delay) 
    {
        yield return new WaitForSeconds(delay);
        Application.Quit(); 
    }
}

public enum InputScheme
{
    Keyboard,
    Controller
}
