using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_InputManager : MonoBehaviour
{
    InputScheme localInputScheme;
    Vector3 cachedMousePosition;

    // Start is called before the first frame update
    void Start()
    {
        cachedMousePosition = Input.mousePosition;
    }

    // Update is called once per frame
    void Update()
    {       
        if (Input.anyKey || Input.mousePosition != cachedMousePosition)
        {      
            localInputScheme = InputScheme.Keyboard;
        }
        cachedMousePosition = Input.mousePosition;

        if (Input.GetKey(KeyCode.JoystickButton0) 
        || Input.GetKey(KeyCode.JoystickButton1)
        || Input.GetKey(KeyCode.JoystickButton2)
        || Input.GetKey(KeyCode.JoystickButton3)
        || Input.GetKey(KeyCode.JoystickButton4)
        || Input.GetKey(KeyCode.JoystickButton5)
        || Input.GetKey(KeyCode.JoystickButton6)
        || Input.GetKey(KeyCode.JoystickButton7)
        || Input.GetKey(KeyCode.JoystickButton8)
        || Input.GetKey(KeyCode.JoystickButton9)
        || Input.GetKey(KeyCode.JoystickButton10)
        || Input.GetKey(KeyCode.JoystickButton11)
        || Input.GetKey(KeyCode.JoystickButton12)
        || Input.GetAxis("LeftAnalogVertical"   ) >   0.1f 
        || Input.GetAxis("LeftAnalogVertical"   ) < - 0.1f 
        || Input.GetAxis("LeftAnalogHorizontal" ) >   0.1f
        || Input.GetAxis("LeftAnalogHorizontal" ) < - 0.1f
        || Input.GetAxis("RightAnalogHorizontal") >   0.1f
        || Input.GetAxis("RightAnalogHorizontal") <  -0.1f
        || Input.GetAxis("RightAnalogVertical"  ) >   0.1f
        || Input.GetAxis("RightAnalogVertical"  ) <  -0.1f)
        {
            localInputScheme = InputScheme.Controller;  
        }

        if (localInputScheme != GameManager.instance.currentInput)
            SetDefaultInput(localInputScheme);
    }

    private void SetDefaultInput(InputScheme inputScheme) 
    {
        switch (inputScheme) 
        {
            case InputScheme.Controller:
            GameManager.instance.currentInput = InputScheme.Controller;
            return;

            case InputScheme.Keyboard:
            GameManager.instance.currentInput = InputScheme.Keyboard;
            return;
        }
    }
}
