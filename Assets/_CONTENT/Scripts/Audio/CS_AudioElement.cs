using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CS_AudioElement
{
    public string name;

    [Range(0.0f, 1.0f)]
    public float volume = 1;

    [Range(-1.0f, 2.0f)]
    public float pitch = 1;

    public bool loop;

    public AudioClip audioClip;

    [HideInInspector]
    public AudioSource audioSource;
}
