using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CS_ActorAudioController : MonoBehaviour
{
    [Header("Attack")]
    [SerializeField] AudioSource attackAudioSource;
    [SerializeField] AudioClip[] onAttackAudioClips;

    [Header("Receive Damage")]
    [SerializeField] AudioSource receiveDamageAudioSource;
    [SerializeField] AudioClip[] onReceiveDamageFxClips;

    [Header("Do Damage")]
    [SerializeField] AudioSource doDamageAudioSource;
    [SerializeField] AudioClip[] onDoDamageFxClips;

    int previousAttackID;
    int previousDoDamageID;
    int previousReceiveDamageID;

    // Start is called before the first frame update
    void Start()
    {
        previousAttackID = 0;   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAttackSFX() 
    {
        if(onAttackAudioClips.Length > 0) 
        {
            int currentAttackID;

            if (previousAttackID + 1 >= onAttackAudioClips.Length) 
            { currentAttackID = 0; }

            else 
            { currentAttackID = previousAttackID + 1; }

            attackAudioSource.PlayOneShot(onAttackAudioClips[currentAttackID]);

            previousAttackID = currentAttackID;
        }
    }

    public void PlayReceiveDamageSFX()
    {
        if (onReceiveDamageFxClips.Length > 0)
        {
            int currentReceiveDamageID;

            if (previousReceiveDamageID + 1 >= onReceiveDamageFxClips.Length)
            { currentReceiveDamageID = 0; }

            else
            { currentReceiveDamageID = previousReceiveDamageID + 1; }

            receiveDamageAudioSource.PlayOneShot(onReceiveDamageFxClips[currentReceiveDamageID]);

            previousReceiveDamageID = currentReceiveDamageID;
        }
    }

    public void PlayDoDamageSFX()
    {
        if (onDoDamageFxClips.Length > 0)
        {
            int currentDoDamageID;

            if (previousDoDamageID + 1 >= onDoDamageFxClips.Length)
            { currentDoDamageID = 0; }

            else
            { currentDoDamageID = previousDoDamageID + 1; }

            doDamageAudioSource.PlayOneShot(onDoDamageFxClips[currentDoDamageID]);

            previousDoDamageID = currentDoDamageID;
        }
    }

    public void PlayEnemyReceiveDamage(AudioClip enemyDamageClip) 
    {
        doDamageAudioSource.PlayOneShot(enemyDamageClip);
    }

    public AudioClip GetEnemyReceiveDamageClip() 
    {
        if(onReceiveDamageFxClips.Length > 0)
            return onReceiveDamageFxClips[Random.Range(0, onReceiveDamageFxClips.Length)];

        else 
            return null;
    }

    public void AudioManagerSfxCallback(string name) 
    {
        Debug.Log("Callback propagation");
        CS_AudioManager.instance.PlayGenericSFX(name);
    }
}
