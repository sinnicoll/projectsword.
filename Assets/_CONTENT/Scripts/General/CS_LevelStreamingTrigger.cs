using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CS_LevelStreamingTrigger : MonoBehaviour
{

    [Header("Scenes")]
    public string sceneToLoad;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {            
            SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).name.Contains("Stage")) 
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).name.Contains("Stage"))
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
            }
        }
    }
}
