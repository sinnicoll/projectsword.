using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_ObjectUninstancer : MonoBehaviour
{
    public float volume;

    // Start is called before the first frame update
    void Start()
    {
        UpdateCachedObjectVolume();
    }

    public void UpdateCachedObjectVolume() 
    {
        float meshSizeX = gameObject.GetComponent<MeshFilter>().mesh.bounds.size.x * transform.localScale.x;
        float meshSizeY = gameObject.GetComponent<MeshFilter>().mesh.bounds.size.y * transform.localScale.y;

        volume = meshSizeX * meshSizeY;

        if (volume < 0.3f) StartCoroutine(DissolveAndDestroyGameObject());
    }

    IEnumerator DissolveAndDestroyGameObject() 
    {
        float timer = 1.0f;

        float localScaleX = this.gameObject.transform.localScale.x;
        float localScaleY = this.gameObject.transform.localScale.y;
        float localScaleZ = this.gameObject.transform.localScale.z;

        while (timer > 0) 
        {
            timer -= Time.deltaTime;
            this.gameObject.transform.localScale = new Vector3(localScaleX * timer, localScaleY * timer, localScaleZ * timer);
            yield return null;
        }
        Destroy(gameObject);
    }
}
