using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CS_GameUtilities
{
    public static void CopyPose(Transform rootFrom, Transform rootTo)
    {
        for(int i = 0; i < rootTo.childCount; ++i)
        {
            rootTo.GetChild(i).position = rootFrom.GetChild(i).position;
            rootTo.GetChild(i).rotation = rootFrom.GetChild(i).rotation;
            if (rootFrom.GetChild(i).childCount > 0) CopyPose(rootFrom.GetChild(i), rootTo.GetChild(i));
        }
    }

    public static Transform GetChildByTag(Transform transform, string tag)
    {
        foreach(Transform child in transform.GetChildren(false))
        {
            //Debug.Log("Transform name: " + child.name + ". Tag name: " + child.tag + ". CompareTag: " + child.CompareTag(tag));
            if (child.CompareTag(tag))
                return child;
            
            if (child.childCount > 0) return GetChildByTag(child, tag);
        }
        return null;
    }

    public static void DrawBoundingBox(Transform toBound, Vector3 size, Color color, float timeActive)
    {
        Vector3 halfSize = size * 0.5f;

        Vector3 p_SouthWestTop = toBound.position - toBound.right * halfSize.x + toBound.up * halfSize.y - toBound.forward * halfSize.z;
        Vector3 p_SouthWestBot = toBound.position - toBound.right * halfSize.x - toBound.up * halfSize.y - toBound.forward * halfSize.z;

        Vector3 p_SouthEastTop = toBound.position + toBound.right * halfSize.x + toBound.up * halfSize.y - toBound.forward * halfSize.z;
        Vector3 p_SouthEastBot = toBound.position + toBound.right * halfSize.x - toBound.up * halfSize.y - toBound.forward * halfSize.z;

        Vector3 p_NorthWestTop = toBound.position - toBound.right * halfSize.x + toBound.up * halfSize.y + toBound.forward * halfSize.z;
        Vector3 p_NorthWestBot = toBound.position - toBound.right * halfSize.x - toBound.up * halfSize.y + toBound.forward * halfSize.z;

        Vector3 p_NorthEastTop = toBound.position + toBound.right * halfSize.x + toBound.up * halfSize.y + toBound.forward * halfSize.z;
        Vector3 p_NorthEastBot = toBound.position + toBound.right * halfSize.x - toBound.up * halfSize.y + toBound.forward * halfSize.z;

        Debug.DrawLine(p_SouthWestTop, p_SouthEastTop, color, timeActive);
        Debug.DrawLine(p_SouthEastTop, p_NorthEastTop, color, timeActive);
        Debug.DrawLine(p_NorthEastTop, p_NorthWestTop, color, timeActive);
        Debug.DrawLine(p_NorthWestTop, p_SouthWestTop, color, timeActive);

        Debug.DrawLine(p_SouthWestBot, p_SouthEastBot, color, timeActive);
        Debug.DrawLine(p_SouthEastBot, p_NorthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthEastBot, p_NorthWestBot, color, timeActive);
        Debug.DrawLine(p_NorthWestBot, p_SouthWestBot, color, timeActive);

        Debug.DrawLine(p_SouthWestTop, p_SouthWestBot, color, timeActive);
        Debug.DrawLine(p_SouthEastTop, p_SouthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthEastTop, p_NorthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthWestTop, p_NorthWestBot, color, timeActive);
    }
    public static void DrawBoundingBox(Vector3 position, Transform toBound, Vector3 size, Color color, float timeActive)
    {
        Vector3 halfSize = size * 0.5f;

        Vector3 p_SouthWestTop = position - toBound.right * halfSize.x + toBound.up * halfSize.y - toBound.forward * halfSize.z;
        Vector3 p_SouthWestBot = position - toBound.right * halfSize.x - toBound.up * halfSize.y - toBound.forward * halfSize.z;

        Vector3 p_SouthEastTop = position + toBound.right * halfSize.x + toBound.up * halfSize.y - toBound.forward * halfSize.z;
        Vector3 p_SouthEastBot = position + toBound.right * halfSize.x - toBound.up * halfSize.y - toBound.forward * halfSize.z;

        Vector3 p_NorthWestTop = position - toBound.right * halfSize.x + toBound.up * halfSize.y + toBound.forward * halfSize.z;
        Vector3 p_NorthWestBot = position - toBound.right * halfSize.x - toBound.up * halfSize.y + toBound.forward * halfSize.z;

        Vector3 p_NorthEastTop = position + toBound.right * halfSize.x + toBound.up * halfSize.y + toBound.forward * halfSize.z;
        Vector3 p_NorthEastBot = position + toBound.right * halfSize.x - toBound.up * halfSize.y + toBound.forward * halfSize.z;

        Debug.DrawLine(p_SouthWestTop, p_SouthEastTop, color, timeActive);
        Debug.DrawLine(p_SouthEastTop, p_NorthEastTop, color, timeActive);
        Debug.DrawLine(p_NorthEastTop, p_NorthWestTop, color, timeActive);
        Debug.DrawLine(p_NorthWestTop, p_SouthWestTop, color, timeActive);

        Debug.DrawLine(p_SouthWestBot, p_SouthEastBot, color, timeActive);
        Debug.DrawLine(p_SouthEastBot, p_NorthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthEastBot, p_NorthWestBot, color, timeActive);
        Debug.DrawLine(p_NorthWestBot, p_SouthWestBot, color, timeActive);

        Debug.DrawLine(p_SouthWestTop, p_SouthWestBot, color, timeActive);
        Debug.DrawLine(p_SouthEastTop, p_SouthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthEastTop, p_NorthEastBot, color, timeActive);
        Debug.DrawLine(p_NorthWestTop, p_NorthWestBot, color, timeActive);
    }

    public static Vector3 Vector3Product(Vector3 a, Vector3 b)
    {
        return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
    }
}
