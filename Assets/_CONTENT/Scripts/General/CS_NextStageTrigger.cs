using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_NextStageTrigger : MonoBehaviour
{
    public string sceneToLoad;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            CS_SceneManager.instance.TransitionToNextLevel(sceneToLoad);
        }
    }
}
