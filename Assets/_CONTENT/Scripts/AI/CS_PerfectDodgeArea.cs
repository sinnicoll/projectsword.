using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_PerfectDodgeArea : MonoBehaviour
{

    [SerializeField]
    BoxCollider col;
    [Header("Gizmo")]
    [SerializeField]
    Color colliderColor;

    private void Awake()
    {
        if(!col)
            col = GetComponent<BoxCollider>();
    }

    public bool isActive { get; protected set; }

    public void SetActivate(bool value)
    {
        isActive = value;
    }

    IEnumerator TimeActiveCor(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        isActive = false;
    }

    public bool CheckIsPerfectDodge()
    {
        //Quaternion rot = new Quaternion(transform.rotation.x * Mathf.Rad2Deg, transform.rotation.y * Mathf.Rad2Deg, transform.rotation.z * Mathf.Rad2Deg, transform.rotation.w * Mathf.Rad2Deg);
        if (isActive)
        {
            Collider[] colsInside = Physics.OverlapBox(col.transform.position + col.center, col.bounds.extents, transform.rotation, LayerMask.GetMask("Player"), QueryTriggerInteraction.Ignore);
            //CS_GameUtilities.DrawBoundingBox(color,.
            foreach(Collider col in colsInside)
            {
                CS_ThirdPersonController player = GameManager.instance.player.GetComponent<CS_ThirdPersonController>();
                if(player)
                {
                    isActive = false;
                    return true;
                }
            }
        }
        return false;
    }


    private void OnDrawGizmos()
    {
        if(col)
        {
            Gizmos.color = colliderColor;
            Gizmos.DrawCube(transform.position + col.center, Vector3.Scale(transform.localScale, col.bounds.size ));
        }
    }
}
