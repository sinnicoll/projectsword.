using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CS_ToggleConmutator : MonoBehaviour
{
    [SerializeField] Toggle toggle_01;
    [SerializeField] Toggle toggle_02;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ConmuteTriggers(int toggleId) 
    {
        if(toggleId == 0 &&  toggle_02.isOn) toggle_02.isOn = false;
        else if(toggleId == 0 && !toggle_02.isOn) toggle_02.isOn = true;

        if(toggleId == 1 &&  toggle_01.isOn) toggle_01.isOn = false;
        else if(toggleId == 1 && !toggle_01.isOn) toggle_01.isOn = true;
    }
}
