using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_WeakPointSystem : MonoBehaviour
{
    [System.Serializable]
    public struct WeakPoint
    {
        public string name;
        public Transform boneWeakPoint;
        public Vector3[] positionsRelativeToBone;
    }

    [SerializeField]
    GameObject weakPointPrefab;

    [SerializeField]
    WeakPoint[] weakPointBones;

    public GameObject weakPoint;

    private void Awake()
    {
        WeakPoint randomWeakPoint = weakPointBones[Random.Range(0, weakPointBones.Length)];
        Vector3   randomPos       = randomWeakPoint.positionsRelativeToBone[Random.Range(0, randomWeakPoint.positionsRelativeToBone.Length)];

        weakPoint = Instantiate(weakPointPrefab, Vector3.zero, weakPointPrefab.transform.rotation, randomWeakPoint.boneWeakPoint);
        weakPoint.transform.localPosition = randomPos;
    }

}
