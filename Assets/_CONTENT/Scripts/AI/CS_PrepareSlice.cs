using BzKovSoft.CharacterSlicer;
using BzKovSoft.ObjectSlicer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CS_PrepareSlice : MonoBehaviour
{
    [SerializeField]
    GameObject ragdollObject;
    [SerializeField]
    GameObject ragdollPrefab;
    [SerializeField]
    bool createRagdollOnPool;
    [SerializeField]
    CS_Pool.SubPoolType enemyType;


    private void Awake()
    {
        if (createRagdollOnPool && !ragdollObject)
            CS_PoolManager._instance.GetPool(CS_Pool.PoolType.RAGDOLL, enemyType).Add();
        else
        {
            ragdollObject = Instantiate(ragdollPrefab);
            ragdollObject.SetActive(false);

            SceneManager.MoveGameObjectToScene(ragdollObject, SceneManager.GetActiveScene());   
        }

    }

    public void Slice(Plane plane, System.Action<BzSliceTryResult> callBack, bool cut)
    {
        if (SpawnRagdoll())
        {
            CS_ControlAIMelee aiScript = GetComponent<CS_ControlAIMelee>();
            if (aiScript)
                aiScript.ChangeHealth(-100);
            BzSliceableCharacterBase slizableChar = ragdollObject.GetComponent<BzSliceableCharacterBase>();
            if(cut) slizableChar.Slice(plane, callBack);
        }
    }

    public GameObject SpawnRagdoll()
    {
        if(!ragdollObject)
            ragdollObject = CS_PoolManager._instance.GetPool(CS_Pool.PoolType.RAGDOLL, enemyType).Spawn(transform.position, transform.rotation, true);
        else
        {
            ragdollObject.SetActive(true);
            ragdollObject.transform.position = transform.position;
            ragdollObject.transform.rotation = transform.rotation;
        }

        Transform transformRoot = CS_GameUtilities.GetChildByTag(transform, "rigRoot");
        Transform ragdollRoot = CS_GameUtilities.GetChildByTag(ragdollObject.transform, "rigRoot");

        if (transformRoot != null && ragdollRoot != null)
        {
            CS_GameUtilities.CopyPose(transformRoot, ragdollRoot);
            gameObject.SetActive(false);
            return ragdollObject;
        }
        else
        {
            if (!transformRoot)
                Debug.LogError("Cannot find \"rigRoot\" tag in " + transform.name + ". Make sure that its hierarchy has this tag on its rig root.");
            if (!ragdollRoot)
                Debug.LogError("Cannot find \"rigRoot\" tag in " + ragdollObject.name + ". Make sure that its hierarchy has this tag on its rig root.");
            return null;
        }


    }
}

