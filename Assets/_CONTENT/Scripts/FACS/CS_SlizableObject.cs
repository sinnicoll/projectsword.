using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BzKovSoft.ObjectSlicer;

public class CS_SlizableObject : BzSliceableObjectBase
{
    protected override void OnSliceFinished(BzSliceTryResult result)
    {
        if (result.sliced)
        {
            CS_FacsSystem playerFacs = GameManager.instance.player.GetComponent<CS_FacsSystem>();

            Rigidbody rbObjNeg = result.outObjectNeg.GetComponentInChildren<Rigidbody>();
            Rigidbody rbObjPos = result.outObjectPos.GetComponentInChildren<Rigidbody>();

            rbObjNeg.AddExplosionForce(playerFacs.explosionForce, playerFacs.facsOrigin.position, 50f, 2f, ForceMode.Impulse);
            rbObjPos.AddExplosionForce(playerFacs.explosionForce, playerFacs.facsOrigin.position, 50f, 2f, ForceMode.Impulse);

            rbObjNeg.isKinematic = false;
            rbObjPos.isKinematic = false;

            Paintable outP_n;
            result.outObjectNeg.TryGetComponent<Paintable>(out outP_n);
            if(outP_n != null) 
            {
                Destroy (outP_n);
            }

            CS_InstanceDeletionPersistance outI_n;
            result.outObjectNeg.TryGetComponent<CS_InstanceDeletionPersistance>(out outI_n);
            if (outI_n != null)
            {
                Destroy(outI_n);
            }

            CS_ObjectUninstancer outU_n;
            result.outObjectNeg.TryGetComponent<CS_ObjectUninstancer>(out outU_n);
            if (outU_n != null)
            {
                outU_n.UpdateCachedObjectVolume();
            }

            Paintable outP_p;
            result.outObjectPos.TryGetComponent<Paintable>(out outP_p);
            if (outP_p != null)
            {
                Destroy(outP_p);
            }

            CS_InstanceDeletionPersistance outI_p;
            result.outObjectPos.TryGetComponent<CS_InstanceDeletionPersistance>(out outI_p);
            if (outI_p != null)
            {
                Destroy(outI_p);
            }

            CS_ObjectUninstancer outU_p;
            result.outObjectPos.TryGetComponent<CS_ObjectUninstancer>(out outU_p);
            if (outU_p != null)
            {
                outU_p.UpdateCachedObjectVolume();
            }

            SceneManager.MoveGameObjectToScene(result.outObjectNeg, SceneManager.GetActiveScene());
            SceneManager.MoveGameObjectToScene(result.outObjectPos, SceneManager.GetActiveScene());
        }
    }
}
