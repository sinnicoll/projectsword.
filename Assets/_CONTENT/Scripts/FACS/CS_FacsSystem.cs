using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using BzKovSoft.CharacterSlicer;
using Invector.vCharacterController.AI;

public class CS_FacsSystem : MonoBehaviour
{
    public enum EnemyDeath
    {
        NORMAL,
        PUSH_AWAY,
        DISMEMBER,
        DISMEMBER_EXPLODE
    }
    public enum ObjectCut
    {
        NO_CUT,
        PUSH_FAR,
        CUT,
        CUT_EXPLODE
    }
    public enum EnemyKillableParts
    {
        ONLY_WEAK_POINTS,
        WHOLE_BODY
    }

    [SerializeField]
    GameObject plane;
    Transform planeCollision;
    public Transform facsOrigin;
    public float explosionForce = 100f;
    public float currentAngle { get; protected set; }

    float planeObjectDepth;
    [SerializeField]
    EnemyDeath currentEnemyDeathType;
    EnemyDeath defVal_enemyDeathType;
    [SerializeField]
    ObjectCut currentObjectCut;
    ObjectCut defVal_objectCut;
    [SerializeField]
    EnemyKillableParts currentKillableParts;
    EnemyKillableParts defVal_killableParts;

    [SerializeField]
    float planeHeight = 1.8f;
    [SerializeField]
    float planeDistance = 1.72f;
    Camera cam;
    Vector3 camDir;

    [Header("- Events - ")]
    public UnityEvent OnStartFacs;
    public UnityEvent OnCut;
    public UnityEvent OnEnemyCut;

    [Header("- References - ")]
    CS_ActorAudioController actorAudioController;

    [SerializeField] ParticleSystem lowLevel_cutPs;
    [SerializeField] ParticleSystem highLevel_cutPs;


    private void Awake()
    {
        plane.transform.parent = null;
        planeCollision = plane.transform.GetChildren(true)[1];

        planeObjectDepth = plane.transform.localScale.z;

        defVal_enemyDeathType = currentEnemyDeathType;
        defVal_objectCut = currentObjectCut;
    }

    private void Start()
    {
        cam = Camera.main;
        actorAudioController = GetComponent<CS_ActorAudioController>();
    }

    private void Update()
    {

    }

    public void SetPlaneDimensions(float width, float depth)
    {
        plane.transform.localScale = new Vector3(width, plane.transform.localScale.y, depth);
    }

    public void SetPlaneObjectDepth(float depth)
    {
        planeObjectDepth = depth;
    }

    public Vector3 GetPlaneSize()
    {
        return plane.transform.localScale;
    }

    public void SetPlaneRotation(float amount)
    {
        camDir = cam.transform.forward;
        plane.transform.position      = transform.position + camDir * planeDistance + new Vector3(0f, planeHeight, 0f);
        plane.transform.forward       = camDir;
        plane.transform.localRotation = Quaternion.Euler(plane.transform.localRotation.eulerAngles.x, cam.transform.parent.localRotation.eulerAngles.y, amount);
    }
    public void ShowPlane(bool value)
    {
        plane.SetActive(value);
    }
    public void Cut(CS_BloodRush bloodRush)
    {        
        //EventSystem 
        OnCut.Invoke();

        currentAngle = plane.transform.localRotation.eulerAngles.z;
        Plane cutPlane = new Plane(plane.transform.up, planeCollision.position);
        Vector3 boxSize = plane.transform.localScale;
        boxSize.y = 0.01f;
        //CS_GameUtilities.DrawBoundingBox(planeCollision, boxSize, Color.blue, 3f);

        Vector3 objectBoxSize = boxSize;
        objectBoxSize.z = planeObjectDepth;

        Vector3 objectBoxPos = plane.transform.position + plane.transform.forward * planeCollision.localPosition.z * (planeObjectDepth / plane.transform.localScale.z);
        CS_GameUtilities.DrawBoundingBox(objectBoxPos, planeCollision, objectBoxSize, Color.cyan, 3f);

        // Check for sliceableObjects
        foreach (Collider col in Physics.OverlapBox(objectBoxPos, objectBoxSize * 0.5f, plane.transform.rotation, LayerMask.GetMask("NonClippable")))
        {
            //Audio component
            CS_ActorAudioController targetAudioController = col.GetComponent<CS_ActorAudioController>();
            if (targetAudioController)
            {
                actorAudioController.PlayEnemyReceiveDamage(targetAudioController.GetEnemyReceiveDamageClip());
            }

            //Persistent deletion component
            CS_InstanceDeletionPersistance targetPersistentDeletionComponent = col.GetComponent<CS_InstanceDeletionPersistance>();
            if (targetPersistentDeletionComponent)
            {
                targetPersistentDeletionComponent.OnDeath();
            }

            CS_SlizableObject slizable = col.GetComponent<CS_SlizableObject>();
            if (slizable)
            {
                if(currentObjectCut != ObjectCut.NO_CUT)
                {
                    slizable.Slice(cutPlane, null);
                    OnEnemyCut.Invoke();
                }
            }
        }

        int mask = -1;

        switch (currentKillableParts)
        {
            case EnemyKillableParts.ONLY_WEAK_POINTS:
                mask = LayerMask.GetMask("WeakPoints");
                break;
            case EnemyKillableParts.WHOLE_BODY:
                mask = LayerMask.GetMask("Enemy");
                break;
        }

        // Check for sliceable Enemies
        foreach (Collider col in Physics.OverlapBox(planeCollision.position, boxSize * 0.5f, plane.transform.rotation, mask, QueryTriggerInteraction.Collide))
        {
            Transform colParent = col.transform;
            if (currentKillableParts == EnemyKillableParts.ONLY_WEAK_POINTS)
            {
                for (int i = 0; i < 20; ++i)
                {
                    colParent = colParent.parent;

                    if (colParent.GetComponent<CS_WeakPointSystem>())
                        i = 20;
                }
            }

            //Audio component
            CS_ActorAudioController targetAudioController = colParent.GetComponent<CS_ActorAudioController>();
            if (targetAudioController) 
            {
                actorAudioController.PlayEnemyReceiveDamage(targetAudioController.GetEnemyReceiveDamageClip());
            }

            //Persistent deletion component
            CS_InstanceDeletionPersistance targetPersistentDeletionComponent = colParent.GetComponent<CS_InstanceDeletionPersistance>();
            if (targetPersistentDeletionComponent)
            {
                targetPersistentDeletionComponent.OnDeath();
            }

            BzSliceableCharacterBase slizableSkin = colParent.GetComponent<BzSliceableCharacterBase>();
            if (slizableSkin)
            {
                vAIMotor ai = slizableSkin.GetComponent<vAIMotor>();
                if (ai) ai.RemoveComponents();
                slizableSkin.Slice(cutPlane, null);

                OnEnemyCut.Invoke();
                GetComponentInChildren<CS_LockOn>().DisableHardLock(colParent.gameObject);


            }
            else
            {
                CS_PrepareSlice slizableChar = colParent.GetComponent<CS_PrepareSlice>();
                if (slizableChar)
                {
                    slizableChar.GetComponent<CS_AICustomEvents>().DetachWeapon();
                    slizableChar.Slice(cutPlane, null, currentEnemyDeathType != EnemyDeath.NORMAL);

                    OnEnemyCut.Invoke();
                    GetComponentInChildren<CS_LockOn>().DisableHardLock(colParent.gameObject);

                    if(bloodRush.currentCharges <= 1)
                        Instantiate(lowLevel_cutPs, colParent.GetComponent<CS_WeakPointSystem>().weakPoint.transform.position, Quaternion.identity);

                    else if(bloodRush.currentCharges == 2)
                        Instantiate(highLevel_cutPs, colParent.GetComponent<CS_WeakPointSystem>().weakPoint.transform.position, Quaternion.identity);

                    bloodRush.AddToCurrentCharge(1);
                }
            }
        }
    }

    public void RestoreDefaultPlayerStats()
    {
        currentEnemyDeathType = defVal_enemyDeathType;
        currentObjectCut = defVal_objectCut;
        currentKillableParts = defVal_killableParts;
    }

    public void UpdateCutTypes(EnemyDeath enemyCutType, ObjectCut objectCutType, EnemyKillableParts killableParts)
    {
        currentEnemyDeathType = enemyCutType;
        currentObjectCut = objectCutType;
        currentKillableParts = killableParts;
    }
}
