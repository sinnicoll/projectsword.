using BzKovSoft.CharacterSlicer.Samples;
using BzKovSoft.ObjectSlicer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_SliceableChar : CharacterSlicerSampleFast
{
    CS_FacsSystem playerFacs;
    [SerializeField] ParticleSystem bloodPs;

    private void Awake()
    {
        playerFacs = GameManager.instance.player.GetComponent<CS_FacsSystem>();
    }

    protected override void OnSliceFinished(BzSliceTryResult result)
    {
        if (result.sliced)
        {
            float heightForce = 0f;

            Transform rbCenterNeg = CS_GameUtilities.GetChildByTag(result.outObjectNeg.transform, "rbCenter");
            Transform rbCenterPos = CS_GameUtilities.GetChildByTag(result.outObjectPos.transform, "rbCenter");

            if (!rbCenterNeg)
                rbCenterNeg = result.outObjectNeg.GetComponentInChildren<Rigidbody>().transform;

            if (!rbCenterPos)
                rbCenterPos = result.outObjectPos.GetComponentInChildren<Rigidbody>().transform;

            rbCenterNeg.GetComponent<Rigidbody>().AddExplosionForce(playerFacs.explosionForce, rbCenterNeg.position + (rbCenterPos.position - rbCenterNeg.position) * 0.5f, 50f, heightForce, ForceMode.Impulse);
            
            if (playerFacs.currentAngle > 45f && playerFacs.currentAngle < 135f || playerFacs.currentAngle < -45f && playerFacs.currentAngle > -135f)
                    rbCenterPos.GetComponent<Rigidbody>().AddExplosionForce(playerFacs.explosionForce, rbCenterPos.position + (rbCenterNeg.position - rbCenterPos.position) * 0.5f, 50f, heightForce, ForceMode.Impulse);

            Instantiate(bloodPs, rbCenterPos);
        }
    }
}
