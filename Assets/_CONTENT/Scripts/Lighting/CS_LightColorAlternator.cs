using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_LightColorAlternator : MonoBehaviour
{
    public float  delayTime;
    public float currentTime;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if(currentTime > delayTime) 
        {
            GetComponent<Light>().color = new Color(1, Random.Range(0.0f, 1.0f), 1);
            currentTime = 0;
        }
    }
}
