using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CS_Pool
{
    public enum PoolType
    {
        NONE,
        RAGDOLL
    }
    public enum SubPoolType
    {
        NONE,
        BASIC_ENEMY
    }

    Transform parent;
    public string name = "P_DefaultPool";
    public PoolType type = PoolType.NONE;
    public SubPoolType subType = SubPoolType.NONE;
    [SerializeField]
    GameObject prefab;
    [SerializeField]
    int numberOfInstances;
    List<GameObject> elements;
    int currentPoolIndex = 0;

    public void Initialize(Transform parent)
    {
        this.parent = parent;
        elements = new List<GameObject>();
        for(int i = 0; i < numberOfInstances; ++i)
        {
            elements.Add(GameObject.Instantiate<GameObject>(prefab, parent));
            elements[i].SetActive(false);
        }
        currentPoolIndex = 0;
    }

    public void Add()
    {
        elements.Add(GameObject.Instantiate<GameObject>(prefab, parent));
        elements[elements.Count-1].SetActive(false);
    }

    public void PullOut(GameObject toPullOut)
    {
        int pullOutIndex = elements.IndexOf(toPullOut);

        if(pullOutIndex >= 0)
        {

        }
        toPullOut.transform.parent = null;
                
        elements.RemoveAt(pullOutIndex);

        if (currentPoolIndex >= pullOutIndex)
            if (currentPoolIndex - 1 <= 0)
            {
                if (elements.Count > 0)
                    currentPoolIndex = elements.Count - 1;
                else
                    currentPoolIndex = 0;
            }
            else --currentPoolIndex;
    }

    public GameObject Spawn(bool pullOut = false)
    {
        return Spawn(Vector3.zero, Quaternion.identity, pullOut);
    }

    public GameObject Spawn(Vector3 position, Quaternion rotation, bool pullOut = false)
    {
        if(elements.Count > 0)
        {
            int indexCache = currentPoolIndex;

            GameObject objectToSpawn = elements[currentPoolIndex];

            do
            {
                if (currentPoolIndex + 1 >= elements.Count)
                    currentPoolIndex = 0;
                else
                    ++currentPoolIndex;

                if (currentPoolIndex == indexCache && elements[currentPoolIndex].activeSelf)
                {
                    Debug.LogError("The pool is currently full. Consider making it longer.");
                    return null;
                }
            } while (elements[currentPoolIndex].activeSelf);

            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;

            if (pullOut)
                PullOut(objectToSpawn);

            return objectToSpawn;
        }

        Debug.LogError("The pool is empty. Spawn could not be done.");
        return null;
        
    }
    
    public bool Despawn(GameObject toDespawn)
    {
        if(elements.Count > 0)
        {
            if (elements.IndexOf(toDespawn) >= 0)
            {
                toDespawn.SetActive(false);
                return true;
            }
            
            Debug.LogError("The pool could not find the GameObject tried to be despawned.");
            return false;
        }

        Debug.Log("The pool is empty. Despawn could not be done.");
        return false;
        
    }

}
