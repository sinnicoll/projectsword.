using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine;

public class CS_InstanceDeletionPersistance : MonoBehaviour
{
    [Header("Persistence")]
    public int persistentID;

    [Header("On Deletion")]
    public GameObject[] spawnOnDeath;

    public UnityEvent onDeath;


    private void Awake()
    {
        persistentID = (int)((transform.position.x + transform.position.y + transform.position.z)*100);

        foreach (var item in GameManager.instance.destroyedComponents)
        {
            if (item == persistentID) 
                this.gameObject.SetActive(false);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDeath()
    {
        if(persistentID != 0)
        GameManager.instance.destroyedComponents.Add(persistentID);

        if(spawnOnDeath != null) 
        {
            foreach (var item in spawnOnDeath) 
            {
                GameObject instance = GameObject.Instantiate(item, this.transform);
                instance.transform.parent = null;

                SceneManager.MoveGameObjectToScene(instance, SceneManager.GetSceneByName(CS_LevelManager.instance.flowSceneName));   
            }
        }
        onDeath.Invoke();
    }
}
