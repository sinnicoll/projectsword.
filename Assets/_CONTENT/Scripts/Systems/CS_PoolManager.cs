using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_PoolManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static CS_PoolManager _instance;
    public static CS_PoolManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CS_PoolManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Bicycle");
                    _instance = container.AddComponent<CS_PoolManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    [SerializeField]
    List<CS_Pool> deadRagdollsPool;
    [SerializeField]
    Transform parent_DeadRagdollsPools;

    private void Awake()
    {
        _instance = this;
        Transform newPoolParent;

        foreach (CS_Pool pool in _instance.deadRagdollsPool)
        {
            newPoolParent = Instantiate(new GameObject(pool.subType.ToString())).transform;
            newPoolParent.transform.parent = parent_DeadRagdollsPools;
            pool.Initialize(newPoolParent);
        }
    }

    public CS_Pool GetPool(CS_Pool.PoolType type, CS_Pool.SubPoolType subType)
    {
        switch (type)
        {
            case CS_Pool.PoolType.NONE:
                return null;

            case CS_Pool.PoolType.RAGDOLL:
                foreach (CS_Pool pool in _instance.deadRagdollsPool)
                    if (pool.subType == subType)
                        return pool;
                return null;

        }
        return null;
    }


}
