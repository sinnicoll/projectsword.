using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CS_CollisionInstancer : MonoBehaviour
{
    [SerializeField] GameObject gameObjectToInstance;
    SphereCollider   coll;

    // Start is called before the first frame update
    void Start()
    {
        coll = this.gameObject.AddComponent<SphereCollider>();
        coll.isTrigger = true;     
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground")) 
        {
            coll.enabled = false;

            GameObject instance = Instantiate(gameObjectToInstance, transform.position, Quaternion.Euler(90, 0, Random.Range(0, 360)));
            instance.transform.parent = null;

            SceneManager.MoveGameObjectToScene(instance, SceneManager.GetSceneByName(CS_LevelManager.instance.flowSceneName));
        }
    }
}
