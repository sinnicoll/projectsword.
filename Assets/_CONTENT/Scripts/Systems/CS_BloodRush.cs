using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CS_BloodRush : MonoBehaviour
{
    [System.Serializable]
    public class BloodRushCharge
    {
        [Tooltip("Just to be identified in the editor, with no importance code-wisely")]
        public string name;

        [Header("Charges")]
        [Tooltip("Last charge in which these stats are applied (last one included)")]
        public int activeToCharge;

        [Header("Movement speed")]
        [Tooltip("Multiplier for the base player speed. The animation speed will NOT be affected by this value")]
        public float movementSpeedMultiplier;
        [Tooltip("Multiplier for the base animation speed. The player speed WILL be affected by this value")]
        public float animationSpeedMultiplier;
        [Tooltip("Multiplier for the base dash speed")]
        public float dashSpeedMultiplier;

        [Header("Perfect Dodge")]
        [Tooltip("Multiplier for the base slow motion effect when executing a perfect dodge")]
        public float slowMoMultiplier;

        [Header("FACS")]
        [Tooltip("Multiplier for the base range of FACS applied to enemies. The range refers to the local Z, or depth, of the FACS plane")]
        public float facsEnemyRangeMultiplier;
        [Tooltip("Multiplier for the base range of FACS applied to objects. The range refers to the local Z, or depth, of the FACS plane")]
        public float facsObjectRangeMultiplier;
        [Tooltip("How the enemies behave when you cut them while using FACS")]
        public CS_FacsSystem.EnemyDeath enemyDeathType;
        [Tooltip("How the object behave when you cut them while using FACS")]
        public CS_FacsSystem.ObjectCut objectCutType;
        [Tooltip("Part of the enemy that kills him when cut with FACS")]
        public CS_FacsSystem.EnemyKillableParts enemyKillableParts;

        [Header("Physics Collision")]
        [Tooltip("How the default rigidbodies behave when colliding against the player")]
        public CS_ThirdPersonController.ObjectCollision collisionType;


        public BloodRushCharge(float movementSpeedMultiplier, float animationSpeedMultiplier, float dashSpeedMultiplier,
            float slowMoMultiplier, float facsEnemyRangeMultiplier, float facsObjectRangeMultiplier, CS_FacsSystem.EnemyDeath enemyDeathType,
            CS_FacsSystem.ObjectCut objectCutType, CS_ThirdPersonController.ObjectCollision collisionType)
        {

            this.movementSpeedMultiplier = movementSpeedMultiplier;
            this.animationSpeedMultiplier = animationSpeedMultiplier;
            this.dashSpeedMultiplier = dashSpeedMultiplier;

            this.slowMoMultiplier = slowMoMultiplier;

            this.facsEnemyRangeMultiplier = facsEnemyRangeMultiplier;
            this.facsObjectRangeMultiplier = facsObjectRangeMultiplier;
            this.enemyDeathType = enemyDeathType;
            this.objectCutType = objectCutType;

            this.collisionType = collisionType;
    }
    }

    [Header("Bloodrush")]
    [Tooltip("How many seconds elapses for each BloodRush charge")]
    public float timePerCharge = 7f;
    [SerializeField]
    [Tooltip("Collection of stats changes per section of charges")]
    BloodRushCharge[] bloodRushStates;
    int currentStateIndex;
    int currentActiveFromCharge;
    [SerializeField]
    [Tooltip("Maximum number of charges the player can accumulate. If exceeded, the timer will be reset to the maximum charge")]
    int maxBloodRushCharges = 9;
    public int currentCharges;

    [Header("UI")]
    [SerializeField]
    Image bloodRushFillImage;
    [SerializeField]
    TextMeshProUGUI chargesText;


    CS_ThirdPersonController player;

    Coroutine resetTimerCor;

    private void Awake()
    {   
        player = transform.parent.parent.GetComponent<CS_ThirdPersonController>();
        transform.parent = null;
    }

    public void AddToCurrentCharge(int charges)
    {
        currentCharges += charges;
        if (currentCharges > maxBloodRushCharges)
            currentCharges = maxBloodRushCharges;

        UpdatePlayerStats(currentCharges);

        string fullChargesText = "x" + currentCharges;
        if (currentCharges >= bloodRushStates.Length)
            fullChargesText += " (MAX)";
        chargesText.text = fullChargesText;

        ResetChargeTimer();
    }

    public void SubstractToCurrentCharge()
    {
        --currentCharges;

        if (currentCharges <= 0)
            currentCharges = 0;
        else
            ResetChargeTimer();

        UpdatePlayerStats(currentCharges);

        string fullChargesText = "x" + currentCharges;
        if (currentCharges >= bloodRushStates.Length)
            fullChargesText += " (MAX)";
        chargesText.text = fullChargesText;
    }

    void UpdatePlayerStats(int charge)
    {
        if (charge <= 0)
            player.RestoreDefaultPlayerStats();
        else
        {
            if(charge > bloodRushStates[currentStateIndex].activeToCharge)
            {
                currentActiveFromCharge = bloodRushStates[currentStateIndex].activeToCharge;
                ++currentStateIndex;
                if (currentStateIndex > bloodRushStates.Length - 1)
                    currentStateIndex = bloodRushStates.Length - 1;
            }
            else if (charge <= currentActiveFromCharge)
            {
                --currentStateIndex;
                if (currentStateIndex <= 0)
                    currentActiveFromCharge = 0;
                else
                    currentActiveFromCharge = bloodRushStates[currentStateIndex - 1].activeToCharge;
            }
            player.ChangePlayerStats(bloodRushStates[currentStateIndex]);
        }
    }

    void ResetChargeTimer()
    {
        if (resetTimerCor != null)
            StopCoroutine(resetTimerCor);
        resetTimerCor = StartCoroutine(ResetChargeTimerCo());
    }

    IEnumerator ResetChargeTimerCo()
    {
        float timeFactor = 1f / timePerCharge; 
        for(float currentTime = timePerCharge; currentTime > 0f; currentTime -= Time.deltaTime)
        {
            bloodRushFillImage.fillAmount = currentTime * timeFactor;
            yield return new WaitForEndOfFrame();
        }

        SubstractToCurrentCharge();
    }

}
