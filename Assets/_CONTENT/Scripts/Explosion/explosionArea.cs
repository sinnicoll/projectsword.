using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionArea : MonoBehaviour
{
    [Header("Explosion settings")]
    [SerializeField]
    public float explosionForce;
    [SerializeField]
    public float explosionRadius;
    [Header("Boom")]
    [SerializeField]
    public bool burst = false;

   
    [SerializeField]
    public SphereCollider sphere = null;
    
    [Header("Debug variables")]
    [SerializeField]
    public bool activeDebug;
    [SerializeField]
    public Color debugColor;

    List<Collider> colliders = new List<Collider>();

    // Start is called before the first frame update
   
    private void OnDrawGizmos()
    {
        
        if(sphere != null)
        {
            sphere.radius = explosionRadius;
        }

        if (activeDebug)
        {           
            Gizmos.color = debugColor;
            Gizmos.DrawSphere(this.transform.position, explosionRadius);
        }
    }

    void Start()
    {
        if (this.GetComponent<SphereCollider>() != null) 
        {
            sphere = this.GetComponent<SphereCollider>();
            sphere.radius = explosionRadius;
            sphere.isTrigger = true;
           
        }
        //if (sphere == null) Debug.LogError("Missing sphere collider triguer component, Please add a sphere collider component as a triguer");


    }
    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {       
        if (!colliders.Contains(other)) colliders.Add(other);   
    }
    private void OnTriggerExit(Collider other)
    {
        if (colliders.Contains(other)) colliders.Remove(other);
    }

    void AddExplosion()
    {
        foreach (Collider collider in colliders) 
        { if (collider.attachedRigidbody != null)
            {
                collider.attachedRigidbody.AddExplosionForce(explosionForce, this.transform.position, explosionRadius, 0f, ForceMode.Impulse);
            }       
        }
        
        burst = false;
    }
}
