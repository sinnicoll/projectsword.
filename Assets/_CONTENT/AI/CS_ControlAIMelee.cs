using System.Collections;
using System.Collections.Generic;
using Invector;
using Invector.vCharacterController.AI;
using UnityEngine;

public class CS_ControlAIMelee : vControlAIMelee
{
    [vEditorToolbar("CS_Attack animations")]
    [SerializeField]
    float prepareAnimationTime = 2f;
    [SerializeField]
    float damageAnimationTime = 0.5f;
    [SerializeField]
    float recoveryAnimationTime = 3f;

    [SerializeField]
    AnimationClip prepareAnimation;
    [SerializeField]
    AnimationClip damageAnimation;
    [SerializeField]
    AnimationClip recoveryAnimation;

    [vEditorToolbar("CS_Attack")]
    [SerializeField]
    float extraCoveredDistance = 2f;
    [SerializeField]
    float maxAttackCooldown = 5f;
    [SerializeField]
    float minAttackCooldown = 3f;

    float attackCooldown;
    public float currentAttackCooldown { get; protected set; }
    public enum CombatState
    {
        IDLE,
        DISTANCING,
        CHASING,
        ATTACKING
    }


    public bool customIsAttacking { get; protected set; }
    [vEditorToolbar("CS_AIGroup")]
    public float distanceToKeepMin = 6f;
    public float distanceToKeepMax = 9f;
    float distanceToKeep;
    public bool isStrafe;
    public int avoidObstaclesRaycasts = 30;
    public float avoidObstaclesDistance = 2f;

    bool isAvoidingObstacle;

    public float distanceToAttack = 4f;

    float timeToDiscardFromAttPoolOnDeath = 3.5f;

    CombatState currentState;
    private void Awake()
    {
        currentAttackCooldown = attackCooldown = Random.Range(minAttackCooldown, maxAttackCooldown);
        isOnAttackCooldown = true;
    }

    protected override void Start()
    {
        base.Start();
        animator.SetFloat("DamageTime", 1f / (damageAnimationTime / damageAnimation.length));
        animator.SetFloat("AnticipationTime", 1f / (prepareAnimationTime / prepareAnimation.length));
        animator.SetFloat("RecoveryTime", 1f / (recoveryAnimationTime / recoveryAnimation.length));

    }

    protected override void Update()
    {
        base.Update();
        UpdateCooldown();

        UpdateAIGroup();
    }


    void UpdateCooldown()
    {
        if (isOnAttackCooldown)
        {
            if (currentAttackCooldown > 0f)
                currentAttackCooldown -= Time.deltaTime;
            else
            {
                currentAttackCooldown = attackCooldown = Random.Range(minAttackCooldown, maxAttackCooldown);
                isOnAttackCooldown = false;
            }
        }
        //Debug.Log("Nombre: " + name + ". Estado: " + currentAttackCooldown);
    }

    public override void Attack(bool strongAttack = false, int _newAttackID = -1, bool forceCanAttack = false)
    {
        customIsAttacking = true;
        isOnAttackCooldown = true;
        animator.SetTrigger("WeakAttack");
        base.Attack(strongAttack, _newAttackID, forceCanAttack);
        StartCoroutine(AttackAnimationSetup());
    }

    IEnumerator AttackAnimationSetup()
    {
        for (float currentTime = 0f; currentTime < prepareAnimationTime; currentTime += Time.deltaTime)
        {
            transform.LookAt(GameManager.instance.player.transform, Vector3.up);
            yield return new WaitForEndOfFrame();
        }

        Vector3 fromAIToEnemy = (GameManager.instance.player.transform.position - transform.position);
        Vector3 fromAIToEnemyNorm = fromAIToEnemy.normalized;
        Vector3 finalPos = fromAIToEnemy + fromAIToEnemyNorm * extraCoveredDistance;
        float speed = (fromAIToEnemy.magnitude + extraCoveredDistance) / damageAnimationTime;

        animator.SetBool("Damaging", true);
        for (float currentTime = 0f; currentTime < damageAnimationTime; currentTime += Time.deltaTime)
        {
            transform.position = (transform.position + fromAIToEnemyNorm * speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(damageAnimationTime);
        animator.SetBool("Damaging", false);
        customIsAttacking = false;
    }

    //------------------ AI Behavior ---------------------------

    public void StartAIGroup()
    {
        ChangeState(CombatState.DISTANCING);
    }

    public void EndAIGroup()
    {
        ChangeState(CombatState.IDLE);
    }

    void UpdateAIGroup()
    {
        UpdateState();
    }
    void ChangeState(CombatState newState)
    {
        switch (currentState)
        {
            case CombatState.IDLE:
                OnIdleExit(newState);
                break;
            case CombatState.DISTANCING:
                OnDistancingExit(newState);
                break;
            case CombatState.CHASING:
                OnChasingExit(newState);
                break;
            case CombatState.ATTACKING:
                OnAttackingExit(newState);
                break;
        }

        switch (newState)
        {
            case CombatState.IDLE:
                OnIdleEnter(currentState);
                break;
            case CombatState.DISTANCING:
                OnDistancingEnter(currentState);
                break;
            case CombatState.CHASING:
                OnChasingEnter(currentState);
                break;
            case CombatState.ATTACKING:
                OnAttackingEnter(currentState);
                break;
        }

        currentState = newState;
    }

    //----------- IDLE -------------
    private void OnIdleEnter(CombatState lastState)
    {
    }
    private void IdleUpdate()
    {
    }

    private void OnIdleExit(CombatState newState)
    {
    }

    //----------- DISTANCING -------------
    private void OnDistancingEnter(CombatState lastState)
    {
        distanceToKeep = Random.Range(distanceToKeepMin, distanceToKeepMax);
    }

    private void DistancingUpdate()
    {
        Vector3 destination = currentTarget.transform.position + (transform.position - currentTarget.transform.position).normalized * distanceToKeep;
        Vector3 moveVector = destination - transform.position;

        moveVector = ModifyObstaclesDirection(moveVector);

        Debug.DrawLine(transform.position, destination, Color.blue, Time.deltaTime);

        StrafeMoveTo(transform.position + moveVector, currentTarget.transform.position - transform.position);

        if (!isOnAttackCooldown && CS_AIManager._instance.IsPreparedToAttack(gameObject))
        {
            ChangeState(CombatState.CHASING);
        }
    }
    private void OnDistancingExit(CombatState newState)
    {
    }

    //----------- CHASING -------------
    private void OnChasingEnter(CombatState lastState)
    {
    }

    private void ChasingUpdate()
    {
        MoveTo(lastTargetPosition, vAIMovementSpeed.Sprinting);
        if (targetDistance <= distanceToAttack)
            ChangeState(CombatState.ATTACKING);

    }

    private void OnChasingExit(CombatState newState)
    {
    }

    //----------- ATTACKING -------------
    private void OnAttackingEnter(CombatState lastState)
    {
        Stop();
        Attack(false, 4);
    }

    private void AttackingUpdate()
    {
        if (!isAttacking)
            ChangeState(CombatState.DISTANCING);
    }

    private void OnAttackingExit(CombatState newState)
    {
        CS_AIManager._instance.TryDiscardAttackingAI(gameObject);
    }
    //----------------------------------


    void UpdateState()
    {
        switch (currentState)
        {
            case CombatState.IDLE:
                IdleUpdate();
                break;
            case CombatState.DISTANCING:
                DistancingUpdate();
                break;
            case CombatState.CHASING:
                ChasingUpdate();
                break;
            case CombatState.ATTACKING:
                AttackingUpdate();
                break;
        }
    }

    Vector3 ModifyObstaclesDirection(Vector3 moveVector)
    {
        Vector3 dir = moveVector;
        float currentAngle = 0f;
        float angleBetween = 360f / avoidObstaclesRaycasts;
        RaycastHit hit = new RaycastHit();

        Vector3 detectionOrigin = transform.position + new Vector3(0f, 0.25f, 0f);
        isAvoidingObstacle = false;

        bool isAvoidingEnemy = false;

        for (int i = 0; i < avoidObstaclesRaycasts; ++i)
        {
            Vector3 tempDir = Quaternion.Euler(0f, currentAngle, 0f) * transform.forward;
            if (Physics.Raycast(detectionOrigin, tempDir, out hit, avoidObstaclesDistance, LayerMask.GetMask("Default", "Enemy"), QueryTriggerInteraction.Ignore))
            {
                if (hit.collider.gameObject.layer == 9) //Enemy
                {
                    Debug.DrawLine(detectionOrigin, transform.position + tempDir * avoidObstaclesDistance, Color.blue, Time.deltaTime);

                    if (isAvoidingEnemy)
                        dir += -tempDir;
                    else
                        dir = -tempDir;
                    dir = dir.normalized;
                    isAvoidingEnemy = true;
                }
                else
                {
                    /* if(!isAvoidingEnemy)
                     {
                         Debug.DrawLine(detectionOrigin, fsmBehaviour.transform.position + tempDir * avoidObstaclesDistance, Color.red, Time.deltaTime);
                         if(!Physics.Raycast(detectionOrigin, Quaternion.Euler(0f, currentAngle + angleBetween, 0f) * fsmBehaviour.transform.forward, out hit, avoidObstaclesDistance, LayerMask.GetMask("Default", "Enemy"), QueryTriggerInteraction.Ignore))
                             dir = Quaternion.Euler(0f, currentAngle + angleBetween, 0f) * fsmBehaviour.transform.forward;
                         Debug.DrawLine(detectionOrigin, fsmBehaviour.transform.position + dir * avoidObstaclesDistance, Color.white, Time.deltaTime);
                     }*/
                }
                isAvoidingObstacle = true;
            }
            else
                Debug.DrawLine(detectionOrigin, transform.position + tempDir * avoidObstaclesDistance, Color.green, Time.deltaTime);

            currentAngle += angleBetween;
        }

        if (isAvoidingObstacle)
        {
            Debug.DrawLine(detectionOrigin, transform.position + dir * avoidObstaclesDistance, Color.black, Time.deltaTime);
            return dir;
        }

        return moveVector;
    }

    public void TryDiscardFromAttackingPool()
    {
        CS_AIManager._instance.TryDiscardAttackingAIAfterSeconds(gameObject, timeToDiscardFromAttPoolOnDeath);
    }


}
