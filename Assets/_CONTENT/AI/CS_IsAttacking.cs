namespace Invector.vCharacterController.AI.FSMBehaviour
{
#if UNITY_EDITOR
    [vFSMHelpbox("Verify if the AI is Attacking", UnityEditor.MessageType.Info)]
#endif

    public class CS_IsAttacking : vStateDecision
    {
        public override string categoryName
        {
            get { return ""; }
        }
        public override string defaultName
        {
            get { return "Is Attacking?"; }
        }

        public override bool Decide(vIFSMBehaviourController fsmBehaviour)
        {
            if (fsmBehaviour.aiController == null) return false;
            return (fsmBehaviour.aiController as CS_ControlAIMelee).customIsAttacking;
        }
    }
}
