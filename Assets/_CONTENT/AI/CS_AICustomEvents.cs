using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_AICustomEvents : MonoBehaviour
{
    [SerializeField]
    GameObject weapon;
    [SerializeField]
    GameObject weaponCollider;
    Invector.vCharacterController.AI.vControlAIMelee ai;
    public CS_PerfectDodgeArea dodgeArea;
    bool rotateToTarget;
    [SerializeField]
    GameObject brillito;

    private void Awake()
    {
        ai = GetComponent<Invector.vCharacterController.AI.vControlAIMelee>();
        if (!dodgeArea)
            dodgeArea = GetComponentInChildren<CS_PerfectDodgeArea>();
    }

    private void Update()
    {
        if(rotateToTarget)
            RotateToTarget();
    }

    public void DetachWeapon()
    {
        if(weapon)
        {
            weapon.transform.parent = null;
            Rigidbody weaponRB = weapon.GetComponent<Rigidbody>();

            if (!weaponRB)
                weaponRB = weapon.AddComponent<Rigidbody>();

            weaponRB.isKinematic = false;

            weaponCollider.SetActive(true);
        }
    }

    protected void RotateToTarget()
    {
        if(ai.currentTarget != null)
        {
            Vector3 targetDirection = ai.currentTarget.transform.position - transform.position;
            targetDirection.y = 0;
            transform.forward = targetDirection;
        }
    }

    public void ActivateRotateToTarget()
    {
        rotateToTarget = true;
    }

    public void DeactivateRotateToTarget()
    {
        rotateToTarget = false;
    }

    public void ActivateDodgeArea()
    {
        dodgeArea.SetActivate(true); 
        ShowAttackIndicator();
    }
    public void DeactivateDodgeArea()
    {
        dodgeArea.SetActivate(false);
        HideAttackIndicator();
    }

    public void ShowAttackIndicator()
    {
        brillito.SetActive(true);
    }

    public void HideAttackIndicator()
    {
        brillito.SetActive(false);
    }

    
}
