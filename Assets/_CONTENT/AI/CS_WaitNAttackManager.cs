using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_WaitNAttackManager : MonoBehaviour
{
    class WaitNAttack_elem
    {
        public CS_ControlAIMelee enemy { get; protected set; }
        public Transform assignedPos { get; protected set; }
        public int currentZone { get; protected set; }

        public WaitNAttack_elem(CS_ControlAIMelee enemy, Transform assignedPos)
        {
            this.enemy = enemy;
            this.assignedPos = assignedPos;
        }
    }

    GameObject playerGO;

    List<WaitNAttack_elem> readyList;
    bool[] zonesState;

    [SerializeField]
    Transform circleRoot;

    [SerializeField]
    float outerCircleRadius;
    [SerializeField]
    float innerCircleRadius;
    [SerializeField]
    int numberOfPositions;
    float degreesForEachPos;

    private void Awake()
    {
        degreesForEachPos = 360f / numberOfPositions;

        playerGO = GameManager.instance.player;


    }

    private void Update()
    {
        circleRoot.position = playerGO.transform.position;
    }

    public int GetNumberOfPositions() { return numberOfPositions; }
    public float GetDegreesForEachPos() { return degreesForEachPos; }
}
