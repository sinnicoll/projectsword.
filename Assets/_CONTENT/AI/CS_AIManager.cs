using Invector.vCharacterController.AI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_AIManager : MonoBehaviour
{

    #region SINGLETON PATTERN
    public static CS_AIManager _instance;
    public static CS_AIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CS_AIManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Bicycle");
                    _instance = container.AddComponent<CS_AIManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public List<GameObject> AIAttacking;

    [SerializeField]
    int maxSimoultaneosAttackingAI = 2;

    private void Awake()
    {
        _instance = this;

        AIAttacking = new List<GameObject>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {/*
        print("-----------------------------");
        foreach (vIControlAICombat ai in AIAttacking)
        {
            print("Name: " + ai.gameObject.name);

        }
        print("-----------------------------");*/
    }

    public bool IsAIAttacking(GameObject ai)
    {
        return _instance.AIAttacking.Contains(ai);
    }

    public GameObject[] GetAttackingAIs()
    {
        return _instance.AIAttacking.ToArray();
    }

    public bool IsPreparedToAttack(GameObject AI)
    {
        if (_instance.AIAttacking.Contains(AI))
            return true;
        if (_instance.AIAttacking.Count < _instance.maxSimoultaneosAttackingAI)
        {
            _instance.AIAttacking.Add(AI);
            return true;
        }
        return false;
    }

    public bool TryAddAttackAI(GameObject AI)
    {
        if(_instance.AIAttacking.Count < _instance.maxSimoultaneosAttackingAI)
        {
            _instance.AIAttacking.Add(AI);
            return true;
        }
        return false;
    }

    public bool TryDiscardAttackingAI(GameObject AI)
    {
        int i = _instance.AIAttacking.IndexOf(AI);

        if (i >= 0)
        {
            _instance.AIAttacking.RemoveAt(i);
            return true;
        }

        return false;
    }

    public bool TryDiscardAttackingAIAfterSeconds(GameObject AI, float seconds)
    {
        int i = _instance.AIAttacking.IndexOf(AI);

        print(i);

        if (i >= 0)
        {
            StartCoroutine(DiscardFromAttPoolCo(i, seconds));
            return true;
        }

        return false;
    }

    IEnumerator DiscardFromAttPoolCo(int index, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        _instance.AIAttacking.RemoveAt(index);
    }
}
