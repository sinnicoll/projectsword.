using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CS_DebugWaitNAttack : MonoBehaviour
{
    [SerializeField]
    CS_WaitNAttackManager waitNAttackSystem;
    [SerializeField]
    Transform circlesRoot;
    [SerializeField]
    GameObject circleReference;

    GameObject[] circles;

    int numberOfPositions;
    float degreesForEachPosition;

    private void Awake()
    {
        numberOfPositions = waitNAttackSystem.GetNumberOfPositions();
        degreesForEachPosition = waitNAttackSystem.GetDegreesForEachPos();

        circles = new GameObject[numberOfPositions];


        circleReference.GetComponent<Image>().fillAmount = degreesForEachPosition / 360f;
        circleReference.transform.rotation = Quaternion.Euler(0f, 0f, degreesForEachPosition * 0.5f) * circlesRoot.rotation;
        circleReference.transform.position += new Vector3(0f, 10f, 0f);
        circles[0] = circleReference;

        for (int i = 1; i < circles.Length; ++i)
        {
            circles[i] = Instantiate<GameObject>(circleReference, circlesRoot);
            circles[i].transform.position = circleReference.transform.position;
            circles[i].transform.position = circlesRoot.position + Quaternion.Euler(0f, 0f, degreesForEachPosition * i) * (circles[i].transform.position - circlesRoot.position);
            circles[i].transform.rotation = Quaternion.Euler(0f, 0f, degreesForEachPosition * 0.5f) * circlesRoot.rotation;
            circles[i].transform.rotation = Quaternion.Euler(0f, 0f, degreesForEachPosition * i) * circles[i].transform.rotation;
        }
    }
}
