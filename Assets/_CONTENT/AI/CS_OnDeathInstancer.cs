using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CS_OnDeathInstancer : MonoBehaviour
{
    [SerializeField] GameObject decalProyector;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InstanceDecalProyector() 
    {
        GameObject instance = GameObject.Instantiate(decalProyector);
        SceneManager.MoveGameObjectToScene(instance, SceneManager.GetSceneByName(CS_LevelManager.instance.flowSceneName));
    }
}
