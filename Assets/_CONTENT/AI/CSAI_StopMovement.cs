using UnityEngine;

namespace Invector.vCharacterController.AI.FSMBehaviour
{
#if UNITY_EDITOR
    [vFSMHelpbox("This is a CSAI_StopMovement Action", UnityEditor.MessageType.Info)]
#endif
    public class CSAI_StopMovement : vStateAction
    {       
       public override string categoryName
        {
            get { return "Movement/"; }
        }
        public override string defaultName
        {
            get { return "Stop Character"; }
        }
        public CSAI_StopMovement()
        {
            executionType = vFSMComponentExecutionType.OnStateUpdate;
        }

        public override void DoAction(vIFSMBehaviourController fsmBehaviour, vFSMComponentExecutionType executionType = vFSMComponentExecutionType.OnStateUpdate)
        {
            StopMovement(fsmBehaviour.aiController as vIControlAICombat);
        }

        void StopMovement(vIControlAICombat aICombat)
        {
            aICombat.Stop();
        }
    }
}