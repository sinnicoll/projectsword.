using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_BloodController : MonoBehaviour
{
    [Header("- Decal Mode - ")]
    [SerializeField] int decalModeAmount;
    [SerializeField] GameObject decalProyector;

    [Header("- UV Paint Mode - ")]
    [SerializeField] int uvPaintAmount;

    [Header("References")]
    ParticleSystem part;
    List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            GameObject blood = Instantiate(decalProyector, collisionEvents[i].intersection, Quaternion.Euler(90, 0, Random.Range(0, 360)));
            blood.transform.parent = null;
            i++;

        }
    }
}
