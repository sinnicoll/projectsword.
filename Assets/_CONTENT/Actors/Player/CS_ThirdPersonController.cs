using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector;

public class CS_ThirdPersonController : vThirdPersonController
{
    public enum ObjectCollision
    {
        NORMAL,
        PUSH_FAR,
        EXPLODE
    }

    protected Coroutine delayTimeRollCo;
    [vEditorToolbar("CS_PerfectDodge")]
    [SerializeField]
    float delayedRollTimeScale = 0.2f;
    [SerializeField]
    float delayedRollDuration = 2.5f;
    public float perfectDodgeDetectionRadius = 3f;
    public bool isDelayTimeRolling { get; protected set; }
    float initialFixedDeltaTime;
    vLockOn lockOnScript;

    [vEditorToolbar("CS_Dash")]
    [SerializeField]
    float dodgeBackwardsAngle = 60;
    [SerializeField]
    float speedAfterDodge = 20f;
    [SerializeField]
    float timeToDecreaseToDefaultSpeed = 1f;
    Coroutine rollSpeedDecreaseCo;
    [SerializeField]
    float rollCooldownSeconds = 0.5f;
    public bool isRollOnCooldown { get; protected set; }
    Coroutine rollCooldownCo;
    bool slowMoOnFacs;

    int endDodgeType = 0;
    float defaultSprintSpeed;

    [vEditorToolbar("CS_Locomotion")]
    [SerializeField]
    float maxAccel = 20f;
    [SerializeField]
    float minAccel = 2f;
    float smoothFactor;

    CS_MeleeCombatInput meleeInput;

    [vEditorToolbar("CS_FACS")]
    [SerializeField]
    float deccelerationOnFacs = 2f;
    [SerializeField]
    float separateCloseEnemyRadius = 1f;
    [SerializeField]
    float separateCloseEnemyForce = 20f;
    Coroutine separatingEnemies;
    CS_FacsSystem facs;


    public vMovementSpeed defVal_defaultFreeSpeed { get; protected set; }
    public vMovementSpeed defVal_defaultStrafeSpeed { get; protected set; }
    public float defVal_animationSpeedMultiplier { get; protected set; }
    public float defVal_dashSpeedMultiplier { get; protected set; }
    public float defVal_slowMoMultiplier { get; protected set; }
    public float defVal_facsEnemyRange { get; protected set; }
    public float defVal_facsObjectRange { get; protected set; }
    public float defVal_speedAfterDodge { get; protected set; }
    public ObjectCollision currentObjectCollision { get; protected set; }



    protected void Awake()
    {
        initialFixedDeltaTime = Time.fixedDeltaTime;
        lockOnScript = GetComponent<vLockOn>();
        defaultSprintSpeed = freeSpeed.sprintSpeed;
        meleeInput = GetComponent<CS_MeleeCombatInput>();

        smoothFactor = ((minAccel - maxAccel) / freeSpeed.sprintSpeed);

        facs = GetComponent<CS_FacsSystem>();
    }

    protected override void Start()
    {
        base.Start();

        defVal_animationSpeedMultiplier = speedMultiplier;
        defVal_dashSpeedMultiplier = animator.GetFloat("dashSpeedMult");
        defVal_defaultFreeSpeed = freeSpeed;
        defVal_defaultStrafeSpeed = strafeSpeed;
        defVal_facsEnemyRange = facs.GetPlaneSize().z;
        defVal_facsObjectRange = facs.GetPlaneSize().z;
        defVal_speedAfterDodge = speedAfterDodge;
        defVal_slowMoMultiplier = delayedRollTimeScale;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody otherRb = collision.collider.GetComponent<Rigidbody>();
        if(otherRb && currentObjectCollision != ObjectCollision.NORMAL && !otherRb.CompareTag("Enemy"))
        {
            float pushForce = 200f;
            if (currentObjectCollision == ObjectCollision.PUSH_FAR)
                otherRb.AddForce(collision.GetContact(0).normal * pushForce, ForceMode.Impulse);
            else
                otherRb.AddForce((collision.GetContact(0).normal + Vector3.up) * pushForce * 5f, ForceMode.Impulse);
        }

    }

    private void Update()
    {
        //print(moveSpeed);
        //print(_rigidbody.velocity);

        if (isRolling)
        {
            Vector3 newInput = new Vector3(meleeInput.horizontalInput.GetAxis(), 0f, meleeInput.verticallInput.GetAxis());
            float angle = Vector3.SignedAngle(Quaternion.Euler(0f, Camera.main.transform.rotation.eulerAngles.y, 0f) * newInput, -transform.forward, Vector3.up);
            
            if (newInput.x < 0.2f && newInput.x > -0.2f && newInput.z < 0.2f && newInput.z > -0.2f)
                endDodgeType = 3; //Stop
            else if (angle <= dodgeBackwardsAngle * 0.5f && angle >= 0)
                endDodgeType = 2; //Right
            else if (angle >= -dodgeBackwardsAngle * 0.5f && angle < 0)
                endDodgeType = 0; //Left
            else
                endDodgeType = 1; //Forward

            animator.SetInteger("dashType", endDodgeType);
        }
    }

    public override void UpdateMotor()
    {
        if (_rigidbody.velocity.magnitude < freeSpeed.sprintSpeed)
        {
            freeSpeed.movementSmooth = _rigidbody.velocity.magnitude * smoothFactor + maxAccel;
            strafeSpeed.movementSmooth = freeSpeed.movementSmooth;
        }
        else
        {

            freeSpeed.movementSmooth = minAccel;
            strafeSpeed.movementSmooth = freeSpeed.movementSmooth;
        }

        base.UpdateMotor();

        //if(!isStrafing)
        //{
            if (!isSprinting)
            {
                if (_rigidbody.velocity.magnitude > runningSpeed * 0.875f)
                    Sprint(true);
            }
            else
            {
                if (_rigidbody.velocity.magnitude <= runningSpeed)
                    Sprint(false);
            }
        //}
    }
    public override void SetControllerMoveSpeed(vMovementSpeed speed)
    {
        base.SetControllerMoveSpeed(speed);
        /*
        if (isCrouching)
        {
            moveSpeed = Mathf.Lerp(moveSpeed, speed.crouchSpeed, currentAcceleration * Time.fixedDeltaTime);
            return;
        }

        if (speed.walkByDefault || alwaysWalkByDefault)
        {
            moveSpeed = Mathf.Lerp(moveSpeed, isSprinting ? speed.runningSpeed : speed.walkSpeed, currentAcceleration * Time.fixedDeltaTime);
        }
        else
        {
            moveSpeed = Mathf.Lerp(moveSpeed, isSprinting ? speed.sprintSpeed : speed.runningSpeed, currentAcceleration * Time.fixedDeltaTime);
        }*/
    }
    protected override void CheckStopMove(ref Vector3 targetVelocity)
    {
        base.CheckStopMove(ref targetVelocity);
        /*
        RaycastHit hit;
        Vector3 origin = transform.position + transform.up * colliderRadiusDefault;
        Vector3 direction = moveDirection.normalized;
        direction = Vector3.ProjectOnPlane(direction, groundHit.normal);
        float distance = colliderRadiusDefault + 1;
        float targetStopWeight = 0;
        float smooth = isStrafing ? strafeSpeed.movementSmooth : currentAcceleration;
        bool checkStopMoveCondition = isGrounded && !isJumping && !isInAirborne && !applyingStepOffset && !customAction;

        if (steepSlopeAhead)
        {
            targetStopWeight = 1f * _slopeSidewaysSmooth;
        }
        else if (checkStopMoveCondition && CheckStopMove(direction, out hit))
        {
            var angle = Vector3.Angle(direction, -hit.normal);
            if (angle < slopeLimit)
            {
                float dst = hit.distance - colliderRadiusDefault;
                targetStopWeight = (1.0f - dst);
            }
            else
            {
                targetStopWeight = -0.01f;
            }

            if (debugWindow)
            {
                Debug.DrawLine(origin, hit.point, Color.cyan);
            }
        }
        else
        {
            targetStopWeight = -0.01f;
        }
        stopMoveWeight = Mathf.Lerp(stopMoveWeight, targetStopWeight, smooth * Time.deltaTime);
        stopMoveWeight = Mathf.Clamp(stopMoveWeight, 0f, 1f);*/

        //targetVelocity = Vector3.LerpUnclamped(targetVelocity, Vector3.zero, currentAcceleration * Time.deltaTime);
    }

    public override void MoveCharacter(Vector3 _direction)
    {
        //base.MoveCharacter(_direction);

        // calculate input smooth
        inputSmooth = Vector3.Lerp(inputSmooth, input, (isStrafing ? strafeSpeed.movementSmooth : freeSpeed.movementSmooth) * (useRootMotion ? vTime.deltaTime : vTime.fixedDeltaTime));
       

        if (isSliding || ragdolled || !isGrounded || isJumping)
        {
            return;
        }

        _direction.y = 0;
        _direction = _direction.normalized * Mathf.Clamp(_direction.magnitude, 0, 1f);
        Vector3 targetPosition = (useRootMotion ? animator.rootPosition : _rigidbody.position) + _direction * (moveSpeed * speedMultiplier) * (useRootMotion ? vTime.deltaTime : vTime.fixedDeltaTime);
        Vector3 targetVelocity = (targetPosition - transform.position) / (useRootMotion ? vTime.deltaTime : vTime.fixedDeltaTime);
        if (inputSmooth.magnitude < 0.1f)
        {
            targetVelocity = Vector3.zero;
        }

        bool useVerticalVelocity = true;

        SnapToGround(ref targetVelocity, ref useVerticalVelocity);

        steepSlopeAhead = CheckForSlope(ref targetVelocity);

        if (!steepSlopeAhead)
        {
            CalculateStepOffset(_direction.normalized, ref targetVelocity, ref useVerticalVelocity);
        }

        CheckStopMove(ref targetVelocity);
        if (useVerticalVelocity)
        {
            targetVelocity.y = _rigidbody.velocity.y;
        }

        _rigidbody.velocity = targetVelocity;
    }
    public override void SetAnimatorMoveSpeed(vMovementSpeed speed)
    {
        base.SetAnimatorMoveSpeed(speed);
    }

    public void OnForwardRollFinished()
    {
        //if(meleeInput.openFacsAfterRoll)
        //{
            meleeInput.openFacsAfterRoll = false;
            if(meleeInput.weakAttackInput.GetButton())
            {
                if (slowMoOnFacs)
                {
                    ChangeTimeScale(delayedRollTimeScale);

                    if (delayTimeRollCo != null)
                        StopCoroutine(delayTimeRollCo);

                    delayTimeRollCo = StartCoroutine(DelayTimeRollCoroutine(false));
                }

                meleeInput.EnterFacs();
                slowMoOnFacs = false;
            }
        //}

        if(endDodgeType == 1 || endDodgeType == 0 || endDodgeType == 2)
        {
            freeSpeed.sprintSpeed = speedAfterDodge;
            moveDirection = transform.forward;
            Sprint(true);
            moveSpeed = speedAfterDodge;
            _rigidbody.velocity = transform.forward * speedAfterDodge;

            animator.SetFloat("InputVertical", 1f);
            animator.SetFloat("InputMagnitude", 1.5f);

            if (rollSpeedDecreaseCo != null)
            {
                StopCoroutine(rollSpeedDecreaseCo);
            }

            rollSpeedDecreaseCo = StartCoroutine(DecreaseSprintSpeedCor());
        }
        /*
        else if(endDodgeType == 0 || endDodgeType == 2)
        {
            moveDirection = transform.forward;
            inputSmooth = input;
            _rigidbody.velocity = transform.forward * speedAfterDodge * 0.4f;
        }*/

        else if(endDodgeType == 3)
        {
            _rigidbody.velocity = Vector3.zero;
        }
    }

    IEnumerator DecreaseSprintSpeedCor()
    {
        float decreaseSpeedFactor = (speedAfterDodge - defaultSprintSpeed) / timeToDecreaseToDefaultSpeed;

        for(float currentTime = 0f; currentTime < timeToDecreaseToDefaultSpeed; currentTime += Time.deltaTime)
        {
            freeSpeed.sprintSpeed -= decreaseSpeedFactor * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        freeSpeed.sprintSpeed = defaultSprintSpeed;
    }

    public virtual void DelayTimeRoll()
    {
        isDelayTimeRolling = true;
        slowMoOnFacs = true;
        print(delayedRollTimeScale);
        ChangeTimeScale(delayedRollTimeScale);

        if (delayTimeRollCo != null)
            StopCoroutine(delayTimeRollCo);

        delayTimeRollCo = StartCoroutine(DelayTimeRollCoroutine(true));
    }

    IEnumerator DelayTimeRollCoroutine(bool rollAfterSlowMo)
    {
        yield return new WaitForSeconds(delayedRollDuration * Time.timeScale);

        ResetTimeScale();
        if(rollAfterSlowMo)
            ChooseRoll();
    }

    public void ExitPerfectDodge()
    {
        if(delayTimeRollCo != null)
        {
            StopCoroutine(delayTimeRollCo);
            isDelayTimeRolling = false;
            ResetTimeScale();
        }
    }

    public virtual void NoTimeRelatedRoll()
    {
        ChooseRoll();
    }

    public virtual void StopDelayTimeAndRoll()
    {
        if (delayTimeRollCo != null)
            StopCoroutine(delayTimeRollCo);

        ResetTimeScale();
        ChooseRoll();
    }

    protected virtual void ChooseRoll()
    {
        isRollOnCooldown = true;

        isDelayTimeRolling = false;

        /*if (_isStrafing)
            LockOnRoll();
        else*/
            Roll();

        if (rollCooldownCo != null)
            StopCoroutine(rollCooldownCo);

        rollCooldownCo = StartCoroutine(RollCooldownCor());

    }

    IEnumerator RollCooldownCor()
    {
        yield return new WaitForSeconds(rollCooldownSeconds);
        isRollOnCooldown = false;
    }

    public override void Roll()
    {
        OnRoll.Invoke();
        isRolling = true;
        if (!meleeInput.isOnPerfectDodge)
            animator.Play("Roll", baseLayer);
        else
        {
            Vector3 dodgeDir = CalculateDodgeDirection(input.x, input.z);

            animator.SetFloat("InputX", dodgeDir.x);
            animator.SetFloat("InputY", dodgeDir.z);

            //print(dodgeDir);

            animator.SetTrigger("ExecPerfectDodge");
        }

        //animator.CrossFadeInFixedTime("Roll", 0f, baseLayer);
        ReduceStamina(rollStamina, false);
        currentStaminaRecoveryDelay = 2f;
    }

    public void LockOnRoll()
    {
        isRolling = true;

        Vector3 dodgeDir = CalculateDodgeDirection(input.x, input.z);

        animator.SetFloat("InputX", dodgeDir.x);
        animator.SetFloat("InputY", dodgeDir.z);

        animator.CrossFadeInFixedTime("Dodge_BlendTree", 0.1f);
        ReduceStamina(rollStamina, false);
        currentStaminaRecoveryDelay = 2f;
    }

    public Vector3 CalculateDodgeDirection(float horizontalInput, float verticalInput)
    {
        Vector3 dodgeDir = new Vector3(horizontalInput, 0f, verticalInput).normalized;
        float rotationDiff = Camera.main.transform.rotation.eulerAngles.y - transform.rotation.eulerAngles.y;

        print("Camera: " + Camera.main.transform.rotation.eulerAngles.y + "; Player: " + transform.rotation.eulerAngles.y);
        print("Before: " + dodgeDir);
        dodgeDir = Quaternion.Euler(0f, rotationDiff, 0f) * dodgeDir;
        print("After: " + dodgeDir + "; rotationDiff: " + rotationDiff);

        return dodgeDir;
    }

    void ChangeTimeScale(float amount)
    {
        Time.timeScale = amount;
        Time.fixedDeltaTime = initialFixedDeltaTime * amount;
    }

    void ResetTimeScale()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = initialFixedDeltaTime;
    }

    public override void StopCharacterWithLerp()
    {
        input = Vector3.Lerp(input, Vector3.zero, deccelerationOnFacs * Time.fixedDeltaTime);
        _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, Vector3.zero, deccelerationOnFacs * 2f * Time.fixedDeltaTime);
        inputMagnitude = Mathf.Lerp(inputMagnitude, 0f, deccelerationOnFacs * Time.fixedDeltaTime);
        moveSpeed = Mathf.Lerp(moveSpeed, 0f, deccelerationOnFacs * Time.fixedDeltaTime);
        animator.SetFloat(vAnimatorParameters.InputMagnitude, 0f, deccelerationOnFacs * 0.1f, Time.fixedDeltaTime);
        animator.SetFloat(vAnimatorParameters.InputVertical, 0f, deccelerationOnFacs * 0.1f, Time.fixedDeltaTime);
        animator.SetFloat(vAnimatorParameters.InputHorizontal, 0f, deccelerationOnFacs * 0.1f, Time.fixedDeltaTime);
        animator.SetFloat(vAnimatorParameters.RotationMagnitude, 0f, deccelerationOnFacs * 0.1f, Time.fixedDeltaTime);
    }

    public void ChangePlayerStats(CS_BloodRush.BloodRushCharge charge)
    {
        speedMultiplier = charge.animationSpeedMultiplier;

        //freeSpeed.walkSpeed = defVal_defaultFreeSpeed.walkSpeed * charge.movementSpeedMultiplier;
        //freeSpeed.runningSpeed = defVal_defaultFreeSpeed.runningSpeed * charge.movementSpeedMultiplier;
        //freeSpeed.sprintSpeed = defVal_defaultFreeSpeed.sprintSpeed * charge.movementSpeedMultiplier;

        //strafeSpeed.walkSpeed = defVal_defaultStrafeSpeed.walkSpeed * charge.movementSpeedMultiplier;
        //strafeSpeed.runningSpeed = defVal_defaultStrafeSpeed.runningSpeed * charge.movementSpeedMultiplier;
        //strafeSpeed.sprintSpeed = defVal_defaultStrafeSpeed.sprintSpeed * charge.movementSpeedMultiplier;

        delayedRollTimeScale = defVal_slowMoMultiplier * (1f / charge.slowMoMultiplier);

        speedAfterDodge = defVal_speedAfterDodge * charge.movementSpeedMultiplier;
        currentObjectCollision = charge.collisionType;

        animator.SetFloat("dashSpeedMult", charge.dashSpeedMultiplier);

        currentObjectCollision = charge.collisionType;

        facs.SetPlaneDimensions(facs.GetPlaneSize().x, defVal_facsEnemyRange * charge.facsEnemyRangeMultiplier);
        facs.SetPlaneObjectDepth(defVal_facsObjectRange * charge.facsObjectRangeMultiplier);

        facs.UpdateCutTypes(charge.enemyDeathType, charge.objectCutType, charge.enemyKillableParts);

    }

    public void RestoreDefaultPlayerStats()
    {
        speedMultiplier = defVal_animationSpeedMultiplier;
        freeSpeed = defVal_defaultFreeSpeed;
        strafeSpeed = defVal_defaultStrafeSpeed;
        speedAfterDodge = defVal_speedAfterDodge;
        delayedRollTimeScale = defVal_slowMoMultiplier;

        animator.SetFloat("dashSpeedMult", defVal_dashSpeedMultiplier);

        currentObjectCollision = ObjectCollision.NORMAL;

        facs.SetPlaneDimensions(facs.GetPlaneSize().x, defVal_facsEnemyRange);
        facs.SetPlaneObjectDepth(defVal_facsObjectRange);
        facs.RestoreDefaultPlayerStats();
    }

    public void SeparateCloseEnemies()
    {
        Collider[] enemiesColliders = Physics.OverlapSphere(transform.position, separateCloseEnemyRadius, LayerMask.GetMask("Enemy"), QueryTriggerInteraction.Ignore);
        
        if(enemiesColliders.Length > 0)
        {
            foreach (Collider enemyCol in enemiesColliders)
            {
                Vector3 forceDirection = enemyCol.transform.position - transform.position;
                forceDirection.y = 0f;
                forceDirection = forceDirection.normalized;
                Rigidbody enemyRb = enemyCol.GetComponent<Rigidbody>();
                enemyRb.AddForceAtPosition(forceDirection * separateCloseEnemyForce * enemyRb.mass, transform.position, ForceMode.Impulse);
            }
        }
    }

    /*IEnumerator SeparateCloseEnemiesCo(Rigidbody[] enemiesRBs)
    {

    }*/

    public void OnDeath() 
    {
        CS_HUD.instance.OnPlayerDeath();
    }
}
