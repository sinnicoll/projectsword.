using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector;
using Invector.vCamera;

public class CS_MeleeCombatInput : vMeleeCombatInput
{
    [vEditorToolbar("FACS")]
    public GenericInput rotateFACS_XInput = new GenericInput("Mouse X", "RightAnalogHorizontal", "MouseX");
    public GenericInput rotateFACS_YInput = new GenericInput("Mouse Y", "RightAnalogVertical", "MouseY");
    public GenericInput cancelFACSInput = new GenericInput("Mouse1", "LB", "LB");
    [SerializeField]
    float timeCamLockedOnFacs = 0.5f;
    [SerializeField]
    float maxTimeInFacs = 3f;
    Coroutine camLockedOnFacsCo;
    CS_FacsSystem facsSystem;
    Vector2 currentRotatePosition;

    float lastFacsAngle;

    public bool onFacs { get; protected set; }
    public bool openFacsAfterRoll { get;  set; }

    public bool isOnPerfectDodge { get; set; }

    GenericInput initialMoveInputX;
    GenericInput initialMoveInputY;

    GenericInput initialCamInputX;
    GenericInput initialCamInputY;

    [vEditorToolbar("BloodRush")]
    [SerializeField]
    CS_BloodRush bloodRush;
    float currentTimeInFacs;

    private void Awake()
    {
        facsSystem = GetComponent<CS_FacsSystem>();
        initialMoveInputX = horizontalInput;
        initialMoveInputY = verticallInput;
        initialCamInputX = rotateCameraXInput;
        initialCamInputY = rotateCameraYInput;
    }

    protected override void Update()
    {
        base.Update();

        if (onFacs && weakAttackInput.GetButtonUp() && MeleeAttackStaminaConditions())
        {
            facsSystem.Cut(bloodRush);
            animator.SetTrigger("facsSlash");
            ExitFacs();
            currentRotatePosition = -currentRotatePosition;
            //TriggerWeakAttack();
        }

        if (onFacs && weakAttackInput.GetButton())
        {
            currentRotatePosition += new Vector2(rotateFACS_XInput.GetAxis() * Time.deltaTime * (1f/Time.timeScale) * 10f, rotateFACS_YInput.GetAxis() * Time.deltaTime * (1f / Time.timeScale) * 10f);
            currentRotatePosition = Vector2.ClampMagnitude(currentRotatePosition, 1f);

            float planeRotation = Vector2.SignedAngle(Vector2.right, currentRotatePosition);
            facsSystem.SetPlaneRotation(planeRotation);

            animator.SetFloat("facsX", currentRotatePosition.x);
            animator.SetFloat("facsY", currentRotatePosition.y);

            if (currentTimeInFacs < maxTimeInFacs)
                currentTimeInFacs += Time.deltaTime;
            else
                ExitFacs();
        }
    }

    public override void MeleeWeakAttackInput()
    {
        if (cc.animator == null)
        {
            return;
        }

        if(!onFacs && weakAttackInput.GetButtonDown())
        {
            EnterFacs();
        }


        if(onFacs && cancelFACSInput.GetButtonUp())
        {
            ExitFacs();
        }

    }

    public void EnterFacs()
    {
        ((CS_ThirdPersonController)cc).SeparateCloseEnemies();

        currentTimeInFacs = 0f;
        CS_CameraManager.instance.FacsPostprocess(true);


        if (isOnPerfectDodge)
        {
            ((CS_ThirdPersonController)cc).ExitPerfectDodge();
            isOnPerfectDodge = false;
        }

        animator.SetBool("FACS", true);
        rotateCameraXInput = horizontalInput;
        rotateCameraYInput = verticallInput;
        lockMoveInput = true;
        cc.StopCharacter();
        facsSystem.ShowPlane(true);
        SetStrafeLocomotion(true);
        Camera.main.transform.parent.GetComponent<vThirdPersonCamera>().ChangeState("Cutting");

        if (camLockedOnFacsCo != null)
        {
            LockCamera(false);
            StopCoroutine(camLockedOnFacsCo);
        }

        LockCamera(true);
        camLockedOnFacsCo = StartCoroutine(LockCamMovForAWhile(timeCamLockedOnFacs));
        StartCoroutine(FacsStopCharacterLerp());        
        
        GetComponentInChildren<CS_LockOn>().SoftLockOn();
        onFacs = true;
    }

    IEnumerator FacsStopCharacterLerp()
    {
        while(onFacs && cc.input != Vector3.zero)
        {
            cc.StopCharacterWithLerp();
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator LockCamMovForAWhile(float seconds)
    {
        LockCamera(true);
        yield return new WaitForSeconds(seconds * Time.timeScale);
        //print("two");
        LockCamera(false);
    }

    void LockCamera(bool value)
    {
        rotateCameraXInput.useInput = !value;
        rotateCameraYInput.useInput = !value;
    }

    void ExitFacs()
    {
        CS_CameraManager.instance.FacsPostprocess(false);
        tpCamera.ChangeState("Default", true);

        if (camLockedOnFacsCo != null)
        {
            LockCamera(false);
            StopCoroutine(camLockedOnFacsCo);
        }
        ExitFacsWithStrafe();
        SetStrafeLocomotion(false);

        EnableStickyness();
    }

    void ExitFacsWithStrafe()
    {
        CS_CameraManager.instance.FacsPostprocess(false);
        tpCamera.ChangeState("Default", true);

        animator.SetBool("FACS", false);
        facsSystem.ShowPlane(false);
        onFacs = false;
        lockMoveInput = false;

        rotateCameraXInput = initialCamInputX;
        rotateCameraYInput = initialCamInputY;

        EnableStickyness();
    }

    public override void TriggerWeakAttack()
    {
        base.TriggerWeakAttack();
    }
    
    void OnPerfectDodge()
    {
        animator.Play("PerfectDodge", 0);
        ((CS_ThirdPersonController)cc).DelayTimeRoll();
    }


    protected override void RollInput()
    {
        if(!((CS_ThirdPersonController)cc).isRollOnCooldown)
        {
            if (rollInput.GetButtonDown() && RollConditions())
            {

                if (onFacs)
                {
                    openFacsAfterRoll = true;
                    ExitFacs();
                    Vector3 dodgeDir = ((CS_ThirdPersonController)cc).CalculateDodgeDirection(horizontalInput.GetAxis(), verticallInput.GetAxis());
                    animator.SetFloat("InputX", dodgeDir.x);
                    animator.SetFloat("InputY", dodgeDir.z);
                }

                Vector3 rollRotation;

                if (verticallInput.GetAxis() != 0f || horizontalInput.GetAxis() != 0f)
                    rollRotation = (Camera.main.transform.forward * verticallInput.GetAxis() + Camera.main.transform.right * horizontalInput.GetAxis()).normalized;
                else
                    rollRotation = Camera.main.transform.forward;
                
                rollRotation.y = 0f;
                transform.forward = rollRotation;

                Collider[] enemiesCols = Physics.OverlapSphere(transform.position, ((CS_ThirdPersonController)cc).perfectDodgeDetectionRadius, LayerMask.GetMask("Enemy"), QueryTriggerInteraction.Ignore);
                foreach (Collider enemyCol in enemiesCols)
                {
                    CS_AICustomEvents aiCustomEvents = enemyCol.GetComponent<CS_AICustomEvents>();
                    if (aiCustomEvents && aiCustomEvents.dodgeArea.CheckIsPerfectDodge())
                    {
                        isOnPerfectDodge = true;
                        OnPerfectDodge();
                        break;
                    }
                }

                if (!isOnPerfectDodge)
                    ((CS_ThirdPersonController)cc).NoTimeRelatedRoll();


                //((CS_ThirdPersonController)cc).DelayTimeRoll();
            }

            else if (isOnPerfectDodge && rollInput.GetButtonUp() && RollConditionsWithoutTimeDelay() && ((CS_ThirdPersonController)cc).isDelayTimeRolling)
            {
                ((CS_ThirdPersonController)cc).StopDelayTimeAndRoll();
                isOnPerfectDodge = false;
            }
        }
    }

    protected override bool RollConditions()
    {
        return (!cc.isRolling || cc.canRollAgain) && !cc.customAction && cc.isGrounded && cc.currentStamina > cc.rollStamina &&
                !cc.isJumping && !cc.animator.IsInTransition(cc.upperBodyLayer) && !cc.animator.IsInTransition(cc.fullbodyLayer) &&
                !((CS_ThirdPersonController)cc).isDelayTimeRolling;
    }

    protected virtual bool RollConditionsWithoutTimeDelay()
    {
        return (!cc.isRolling || cc.canRollAgain) && !cc.customAction && cc.isGrounded && cc.currentStamina > cc.rollStamina &&
                !cc.isJumping && !cc.animator.IsInTransition(cc.upperBodyLayer) && !cc.animator.IsInTransition(cc.fullbodyLayer);
    }
}
