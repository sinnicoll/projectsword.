using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CS_LockOn : vLockOn
{
    new public vMeleeCombatInput tpInput;

    private void Awake()
    {
        tpInput = GetComponentInParent<vMeleeCombatInput>();
    }
    private void Update()
    {
        if( currentTarget != null) 
        {
            
        }
    }

    protected override void LockOnInput()
    {
        if (tpInput.tpCamera == null || tpInput.cc == null) return;

        // lock the camera into a target, if there is any around
        if (lockOnInput.GetButtonDown() && !tpInput.cc.customAction)
        {
            isLockingOn = !isLockingOn;
            LockOn(isLockingOn);
        }

        // unlock the camera if the target is null
        else if (isLockingOn && (tpInput.tpCamera.lockTarget == null))
        {
            isLockingOn = false;
            LockOn(false);
        }

        // choose to use lock-on with strafe or free movement
        if (strafeWhileLockOn && !tpInput.cc.locomotionType.Equals(vThirdPersonMotor.LocomotionType.OnlyStrafe))
        {
            if (isLockingOn && tpInput.tpCamera.lockTarget != null)
            {
                tpInput.cc.lockInStrafe = true;
                tpInput.cc.isStrafing = true;
            }
        }
    }

    public void DisableHardLock(GameObject reference) 
    {
        if(currentTarget != null && currentTarget.name.Contains(reference.name))
        {
            LockOn(false);
        }
    }

    public void SoftLockOn() 
    {
        if (GetPossibleTargets().Count > 0) 
        {
            if(Vector3.Distance(this.transform.position, GetPossibleTargets()[0].position) > 1.0f) 
            {
                //Debug.Log(Vector3.Distance(this.transform.position, GetPossibleTargets()[0].position));
                Vector3 relativePos = GetPossibleTargets()[0].position - this.transform.position;

                float targetAngle = Quaternion.LookRotation(relativePos, Vector3.up).eulerAngles.y;
                float playerAngle = tpInput.tpCamera.transform.rotation.eulerAngles.y;            

                tpInput.tpCamera.RotateCamera((targetAngle - playerAngle) / tpInput.tpCamera.currentState.xMouseSensitivity, 0);            
            }
        }
    }
}
