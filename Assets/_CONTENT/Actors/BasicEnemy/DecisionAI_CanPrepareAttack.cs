using UnityEngine;

namespace Invector.vCharacterController.AI.FSMBehaviour
{
#if UNITY_EDITOR
    [vFSMHelpbox("This is a DecisionAI_CanPrepareAttack decision", UnityEditor.MessageType.Info)]
#endif
    public class DecisionAI_CanPrepareAttack : vStateDecision
    {
		public override string categoryName
        {
            get { return "MyCustomDecisions/"; }
        }

        public override string defaultName
        {
            get { return "DecisionAI_CanPrepareAttack"; }
        }

        public override bool Decide(vIFSMBehaviourController fsmBehaviour)
        {
            //Debug.Log(fsmBehaviour.gameObject);
            if (fsmBehaviour.aiController == null)
                return false;
            if (fsmBehaviour.aiController is vIControlAICombat)
            {
                CS_ControlAIMelee ai = fsmBehaviour.gameObject.GetComponent<CS_ControlAIMelee>();
                if (ai != null)
                {
                    if (!ai.isOnAttackCooldown && CS_AIManager._instance.IsPreparedToAttack(ai.gameObject))
                    {
                        //Debug.Log("Name: " + ai.name + ", Cooldown: " + ai.currentAttackCooldown);
                        return true;
                    }
                    return false;
                    // return !ai.isOnAttackCooldown && CS_AIManager._instance.IsPreparedToAttack(ai.gameObject);
                }
                return false;
            }
            return false;
        }
    }
}
