using System;
using UnityEngine;

namespace Invector.vCharacterController.AI.FSMBehaviour
{
#if UNITY_EDITOR
    [vFSMHelpbox("This is a ActionAI_KeepDistanceToEnemy Action", UnityEditor.MessageType.Info)]
#endif
    public class ActionAI_KeepDistanceToEnemy : vStateAction
    {
        public override string categoryName
        {
            get { return "MyCustomActions/"; }
        }
        public override string defaultName
        {
            get { return "Keep Distance To Enemy"; }
        }

        public override void DoAction(vIFSMBehaviourController fsmBehaviour, vFSMComponentExecutionType executionType = vFSMComponentExecutionType.OnStateUpdate)
        {
            switch (executionType)
            {
                case vFSMComponentExecutionType.OnStateEnter:
                    fsmBehaviour.aiController.gameObject.GetComponent<CS_ControlAIMelee>().StartAIGroup();
                    break;
                case vFSMComponentExecutionType.OnStateUpdate:
                    break;
                case vFSMComponentExecutionType.OnStateExit:
                    fsmBehaviour.aiController.gameObject.GetComponent<CS_ControlAIMelee>().EndAIGroup();
                    break;
            }
        }
    }
}