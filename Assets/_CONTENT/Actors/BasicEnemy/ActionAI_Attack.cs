using UnityEngine;

namespace Invector.vCharacterController.AI.FSMBehaviour
{
#if UNITY_EDITOR
    [vFSMHelpbox("This is a ActionAI_Attack Action", UnityEditor.MessageType.Info)]
#endif
    public class ActionAI_Attack : vStateAction
    {
        vIControlAICombat aiScript;

        public bool overrideAttackID;
        [vHideInInspector("overrideAttackID")]
        public int attackID;
        public bool overrideStrongAttack;
        [vHideInInspector("overrideStrongAttack")]
        public bool strongAttack;
        public override string categoryName
        {
            get { return "MyCustomActions/"; }
        }
        public override string defaultName
        {
            get { return "ActionAI_Attack"; }
        }

        public override void DoAction(vIFSMBehaviourController fsmBehaviour, vFSMComponentExecutionType executionType = vFSMComponentExecutionType.OnStateUpdate)
        {
            switch (executionType)
            {
                case vFSMComponentExecutionType.OnStateEnter:
                    OnStateEnter(fsmBehaviour);
                    break;
                    /*
                case vFSMComponentExecutionType.OnStateUpdate:
                    KeepDistanceToPlayer(fsmBehaviour, GameManager.instance.player);
                    break;*/
                    
                case vFSMComponentExecutionType.OnStateExit:
                    OnStateExit(fsmBehaviour);
                    break;

            }
        }

        void OnStateEnter(vIFSMBehaviourController fsmBehaviour)
        {
            if(fsmBehaviour.aiController is vIControlAICombat)
            {
                if (aiScript == null)
                    aiScript = (fsmBehaviour.aiController as vIControlAICombat);

                Debug.Log("Name: " + aiScript.gameObject.name);
                aiScript.Attack(overrideStrongAttack ? strongAttack : false, overrideAttackID ? attackID : -1);

            }
            
        }

        void OnStateExit(vIFSMBehaviourController fsmBehaviour)
        {
            CS_AIManager._instance.TryDiscardAttackingAI(aiScript.gameObject);
        }
    }
}